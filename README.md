# README #

## Portable Emacs Configuration ##

* These files are my init files for emacs. Version 26+ is needed to
  guarantee that everything will work.
* Version 1.0

### Setup ###

* Clone the repo into `~/.emacs.d`
* Rename the directory to `~/.emacs.d/.emacs-config`
* Copy the `init.el` file into `~/.emacs.d`
* This configuration uses `use-package` to manage package loading and
  dependencies. The first time emacs is started with these files,
  `use-package` will attempt to download and install all the
  packages. Network issues may result in some failures. These will
  have to be installed by hand. Easiest way I've found is to locate
  the `use-package` directive for the offender and `C-x C-e` to reload
  it. `use-package` will attempt to install the package again, and
  this usually works. All packages are set to `:ensure t` to force
  this behavior.
* If changes are made to `~/.emacs.d/.emacs-config/init.el`, the changed
  file is copied to `~/.emacs.d` on exit. This is the preferred method.
* Files `.emacs-config.el`, `.emacs-config-vars.el`, and `.prog-modes.el`
  are byte-compiled. Emacs on startup will detect changes to the
  source files and recompile them before loading them. This impacts load
  time. This impact can be avoided by manually byte-compiling them before
  restarting emacs.
* Creation of new vars should be done in `.emacs-config-vars.el`. The
  other files `require` this one.
* Toggle any editing modes as needed or not. See below.
* Bookmarks are used as a table of contents for navigating the
  files. A file's bookmarks are listed at the top of the file. `C-x r
  b` to navigate. Helm will display all bookmarks in a buffer.
* Settings via `Customize` have been put into their own file,
  `custom.el`. This file is loaded through `init.el`.
* Org files are stored in Google Drive. The locations are set via the
  variables `org-agenda-files` (`setq` in `.emacs-config.el`) and
  `org-capture-templates`. The latter currently resides in
  `custom.el`.

### (Some of) What's Included ###

* **treemacs** (Project-based file tree similar to VS Code)
* **helm** (provides popup menu interface for common actions, such as
  changing buffers, looking up context-based documentation)
* **org mode** The go-to GTD tool
* **Magit** (interface to git, every command you'll ever need)
* **Projectile** (project management)
* **company** ("complete anything")
* **which key** (keybinding completion popup. Never fret over a key
  chord again.)
* Custom top-level menu, `Extensions`, with custom option list (e.g.,
  *info emacs*, *info elisp*, *get Daily ToDo List*). This menu is
  created in Emacs lisp and can be changed that way.
* **Desktop mode** (remembers buffers, window and file positions)
* **elpy** (Python IDE, *enabled by default*)
* **Rust mode** (*enabled by default*)
* **JS2** JavaScript IDE (*enabled by default*)
* **cc-mode** (C/C++ mode, disabled by default because it's huge)
* **ESS** (*Emacs Speaks Statistics* with R, disabled by default)
* **JDEE** (*Java Development Environment for Emacs*, disabled by
  default)
* **Quack** (mode for Scheme, disabled by default)
* **SLIME** (*Superior Lisp Interaction Mode for Emacs*) for Common
  Lisp (disabled by default)
* **tide** (TypeScript IDE, disabled by default)

### What's Not Included ###

* External programs such as *Python*, *Scheme*, *Rust*, *R*, *Common
  Lisp*, *git*

### Gotchas ###

* Locations of many external files and programs are hard-coded via
  `Customize` or the init files themselves. *They'll probably need to
  be updated*.
* Current load time is ~35 seconds. 
