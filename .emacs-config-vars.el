;;; .emacs-config-vars.el --- Collection of variables to be used in load files. -*- coding: utf-8 -*-; lexical-binding: t -*-
;;; Time-stamp: <Thu Sep  9, 2021 11:53 AM micha>
;;; File name: .emacs-config-vars.el
;;; Created Mon Aug 31, 2015 10:35 AM
;;; File to contain variable definitions for the local emacs
;;;
;;; Tue Oct 16, 2012 12:40 PM
;;; location of the editable version of the .emacs
;;; this file is in CVS.  save-hook has a function
;;; to automatically copy an updated version of this file
;;; into place.
;;; Updated for Windows 7 Wed Jun 26, 2013  7:55 AM
;;;
;;; Commentary:
;;;
;;;
;;; Code:

;; Fri Nov  5, 2021 11:44 PM
;; variables
(defvar org-capture-templates)
(defvar org-clock-persist)
(defvar more-languages)
(defvar default-major-mode)
(defvar whitespace-style)
(defvar whitespace-line-column)
(defvar buffer-face-mode-face)
(defvar helm-completing-read-handlers-alist)
(defvar company-active-map)
(defvar lsp-completion-enable-additional-text-edit)
;;
;; functions
(declare-function treemacs-filewatch-mode "treemacs")
(declare-function tramp-set-completion-function "tramp")
(declare-function org-entry-delete "org")
(declare-function org-back-to-heading "org")

;;; Thu Jul  1, 2021  2:17 AM
(defvar use-cc-mode nil
"Enable or disable the loading of the cc-mode module.
It only needs to be loaded when doing C programming.")
;;
(defvar my-use-package-csharp-p nil "Enable or disable packages for coding in C#.")
(defvar my-use-package-clojure-p nil "Enable or disable loading CIDER for coding in clojure.")
(defvar my-use-package-dart-p nil "Enable or disable loading packages for coding in Dart and Flutter.")
(defvar my-use-package-docker-p nil "Enable or disable packages for coding in docker.")
(defvar my-use-package-ess-p nil "Enable or disable loading ESS for statistics with R.")
(defvar my-use-package-fsharp-p nil "Enable or disable loading packages for coding in F#.")
(defvar my-use-package-go-mode-p nil "Enable or disable packages for coding in Go.")
(defvar my-use-package-haskell-p nil "Enable or disable loading packages for coding in Haskell.")
(defvar my-use-package-javascript-p nil "Enable or disable loading packages for coding in JavaScript.")
(defvar my-use-package-jdee-p nil "Enable or disable loading JDEE for coding in Java.")
(defvar my-use-package-jmt-mode-p nil "Enable or disable the Java Mode Tamed package for coding in Java.")
(defvar my-use-package-julia-p nil "Enable or disable loading packages for coding in julia.")
(defvar my-use-package-meghanada-p nil "Enable or disable meghanada packages for coding in Java.")
(defvar my-use-package-nim-p nil "Enable or disable packages for coding in nim.")
(defvar my-use-package-php-p nil "Enable or disable loading packages for coding in php.")
(defvar my-use-package-powershell-p nil "Enable or disable loading packages for coding in PowerShell.")
(defvar my-use-package-python-p nil "Enable or disable loading ELPY and related packages for coding in Python.")
(defvar my-use-package-quack-p nil "Enable or disable loading Quack for coding in Scheme.")
(defvar my-use-package-rust-p nil "Enable or disable loading packages for coding in Rust.")
(defvar my-use-package-scala-p nil "Enable or disable packages for coding in Scala.")
(defvar my-use-package-slime-p nil "Enable or disable loading SLIME for coding in Common Lisp.")
(defvar my-use-package-tide-p nil "Enable or disable loading TIDE for editing TypeScript.")
(defvar my-use-package-web-p nil "Enable or disable loading packages for coding web-related functions.")

;; various interpreters and compilers
;; Wed Jul 14, 2021 10:52 PM
(cond
 ((eq system-type 'gnu/linux )
  (progn
    (defvar clojure-path "/usr/local/bin/clj" "Path to the local clojure exe in Linux.")
    (defvar clisp-path "/usr/bin/clisp" "Path to the Common Lisp interpreter in Linux.")
    (defvar scheme-path "usr/bin/scheme48" "Path to the Scheme interpreter in Linux." )
    (defvar guile-path "/usr/bin/guile" "Path to the Guile interpreter in Linux." )
    (defvar go-path "/usr/bin/go" "Path to the go compiler in Linux.")
    (message "Linux paths set.")
    ))
 ((eq system-type 'windows-nt )
  (progn
    (defvar clojure-path "C:/ProgramData/chocolatey/bin/clj.bat" "Path to the clojure exe in Windows.")
    (defvar scheme-path "C:/Program Files/Chez Scheme 9.5.4/bin/ta6nt/scheme.exe" "Path to the Scheme interpreter in Windows.")
    (defvar clisp-path "C:/tools/clisp-2.49/clisp.exe" "Path to the Common Lisp interpreter in Windows.")
    (defvar gambit-path "c:/Program Files/Gambit/v4.9.3/bin/gsi.exe" "Path to the Gambit interpreter in Windows.")
    (defvar go-lang-path " C:/Program Files/Go/bin/go.exe" "Path to the Go compiler in Windows.")
    (defvar ruby-path "C:/tools/ruby30/bin/irb.cmd" "Path to Ruby interpreter in Windows.")
    (message "Windows paths set.")
    ))
 (t (message "Something went wrong in setting directories.")))

;; ctags and org files paths
;; Wed Jul 14, 2021 10:52 PM
(cond
 ((eq system-type 'gnu/linux)
  (progn
    (defvar ctags-path "/usr/local/bin/ctags")
    (defvar org-files-path "~/Gdrive/org" "The path to Google Drive on the Linux system. ")
    (message (format "Linux org path set to `%s'." org-files-path))))
 ((eq system-type 'windows-nt)
  (progn
    (defvar ctags-path "c:/Emacs/x86_64/bin/ctags.exe")
    (defvar org-files-path "G:/My Drive/org" "The path to Google Drive on the Windows system.")
    (message (format "Windows org path set to `%s'." org-files-path)))))

;; org files
;; Sat Jul 31, 2021  6:14 AM
(cond
 ((eq system-type 'gnu/linux)
  (progn
    (defvar org-books-file-linux "~/Gdrive/org/books.org")
    (message "Linux org-books file set.")))
 ((eq system-type 'windows-nt)
  (progn
    (defvar org-books-file-windows "G:/My Drive/org/books/books.org")
    (message "Windows org-books file set."))))

;;
;; Sat Nov 10, 2012  8:34 PM
;; For alternative javascript (jrunscript)
;;
;; (defvar jrun-path "C:\\\\Program Files\\\\Java\\\\jdk1.7.0_07\\\\bin\\\\jrunscript.exe"
;;   "Path to the Oracle version of the Javascript shell.")
;;
;;
;; Wed Oct 19, 2011  7:37 AM
;; Symbols to identify directories and files to be loaded
;; so we can check for existence and message about missing files
;;
(defvar local-site-lisp "~/share/emacs/site-lisp/")
(defvar local-lisp "~/share/emacs/lisp/")
(defvar local-share-lisp "~/share/lisp/")
;;
(defvar my-scala-program-name "C:/Program Files (x86)/scala/bin/scala.bat")
;; get rid of `free-variable' warning, it's annoying
;; why is this variable not found with Ch v?
(defvar inferior-lisp-program "clisp")
;; Mon Oct 15, 2012 10:18 AM
(defvar powershell-prompt-regex  ".* -->")

;; fool the linter into thinking these are defined and not "free
;; variables." `defvar' has no effect on vars already defined.
;; Sat Aug 14, 2021 11:51 AM
(defvar calendar-latitude)
(defvar calendar-longitude)
(defvar calendar-location-name)
;;
;; configure settings for calendar to report local data
;; These are the values for the homestead
;; Sat Aug 14, 2021 11:48 AM
(setq calendar-latitude 41.5)
(setq calendar-longitude -73.1)
(setq calendar-location-name "Naugatuck, CT")

(provide '.emacs-config-vars)
;;; .emacs-config-vars.el ends here
