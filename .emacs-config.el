;;; .emacs-config.el --- Miscellaneous configuration options -*- coding: utf-8; lexical-binding: t; -*-
;;; File: .emacs-config.el
;;;
;;; Mon Nov 15, 2021  5:22 PM
;;;
;;; Commentary:
;;;
;;; This file contains configuration options for general
;;; editing and for Emacs itself.
;;;
;;; Author: mpowe
;;; Date: 2003/06/13 22:09:15
;;; Revision: 1.2.1.161
;;;
;;; $Author: powem $
;;; $Date: 2012/11/09 07:01:59 $
;;; $Revision: 1.5 $
;;;
;;; Emacs configuration files have been moved to their own
;;; directory, so that it is possible to keep them in version control
;;; Sun Aug 30, 2015  9:04 AM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Tue Jun 29, 2021  7:45 PM
;;; Some loading has been wrapped in `with-suppressed-warnings' to get
;;; rid of warnings during byte compilation.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;		      Initial setup for globals
;;;		      Sun Jan 31, 2021 10:31 AM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Code:
;;;
(require '.emacs-config-vars)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;	    set default-directory according to system type
;;;		      Sun Jan 31, 2021 12:13 PM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; This becomes buffer-local when set, so keep it here rather than
;; moving it to `.emacs-config-vars'
;; Wed Jul 14, 2021  8:56 PM
;;
(cond
 ((eq system-type 'windows-nt)
  (setq default-directory (format "c:/Users/%s/AppData/Roaming/" user-login-name)))
 ((eq system-type 'gnu/linux)
  (setq default-directory "~/")
  )
 (t (message "Setting of `default-directory' failed")))

(use-package diminish :defer t)
;;
;; run these after the init loading is done. They seem not to run when
;; placed standalone in the init file.
;;
;; Use this hook function to run the commands. Lambda functions are
;; not (necessarily) good here, because the lambda wraps its internals
;; in a function object. If the first thing it encounters does not
;; look like a function, it will give the `malformed function' error.
;; Thu Oct 28, 2021 11:29 AM
;;
(defun my-diminish-hook ()
     (diminish 'helm-mode "|H|")
     (diminish 'zettelkasten-mode "|Z|")
     (diminish 'abbrev-mode "abv")
     (diminish 'projectile-mode "Proj")
     (diminish 'buffer-face-mode "Bface")
     (diminish 'org-fancy-priorities-mode "FancyP")
     (diminish 'helm-mode "|H|")
     (diminish 'company-mode "Co")
     (diminish 'git-gutter-mode "GitG"))

(add-hook 'after-init-hook 'my-diminish-hook)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;			    begin org mode
;;;		      Tue Oct 25, 2011  2:07 PM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Updated to use version direct from dev repo
;; Will that be an "upgrade"?
;; Tue Nov 16, 2021 10:07 AM
(use-package org
  :load-path "~/.emacs.d/org-new/org-mode/lisp"
  :config
  (add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
  (global-set-key "\C-cl" 'org-store-link)
  (global-set-key "\C-ca" 'org-agenda)
  (global-set-key "\C-cn" 'org-capture)
  (global-set-key "\C-cg" 'org-gcal-sync)
  (bind-key "C-c C-!" 'org-time-stamp-inactive org-mode-map))
;;
;; clock persistence across sessions
;; Sun Jan 31, 2021 11:14 AM
(with-eval-after-load "org"
  (setq org-clock-persist 'history)
  (org-clock-persistence-insinuate)
  (setq org-log-done 'note))

;; show diary settings in agenda view
;; Sun Feb 27, 2022 11:02 AM
(setq org-agenda-include-diary t)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;		   location of agenda files, by OS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(cond
 ((eq system-type 'gnu/linux)
  (progn
    (setq org-agenda-files
          '("~/Gdrive/org/calendar.org" "~/Gdrive/org/tasks.org"
            "~/Gdrive/org/daily.org" "~/Gdrive/org/chores.org"
            "~/Gdrive/org/chess.org" "Gdrive/org/study.org"))
    (message (format "Linux org path set to `%s'." org-files-path))))
 ((eq system-type 'windows-nt)
  (progn
    (setq org-agenda-files
          '("g:/My Drive/org/calendar.org" "g:/My Drive/org/tasks.org"
            "g:/My Drive/org/daily.org" "g:/My Drive/org/chores.org"
            "g:/My Drive/org/chess.org" "g:/My Drive/org/study.org"))
    (message (format "Windows org path set to `%s'." org-files-path)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;		 location of capture templates, by OS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(cond
 ((eq system-type 'gnu/linux)
  (progn
    (setq org-capture-templates
          '(("b" "New Book" entry
             (file+headline "~/Gdrive/org/books/books.org" "New Items")
             (file "~/Gdrive/org/books/books-tmpl.txt")
             :empty-lines-after 1)
            ("t" "New ToDo Item (Unscheduled)" entry
             (file+headline "~/Gdrive/org/daily.org" "Captures")
             (file "~/Gdrive/org/templates/todo-tmpl.txt")
             :empty-lines-after 1)
            ("n" "New Note" entry
             (file "~/Gdrive/org/notes.org")
             (file "~/Gdrive/org/templates/note-tmpl.txt")
             :empty-lines-after 1)
            ("p" "Plain Text Note" plain
             (file "~/Gdrive/org/notes.org")
             (file "~/Gdrive/org/templates/note-plain-tmpl.txt")
             :empty-lines-before 1 :empty-lines-after 1)
            ("j" "Journal Entry" entry
             (file+olp+datetree "~/Gdrive/org/journal.org")
             (file "~/Gdrive/org/templates/journal-tmpl.txt"))
            ("r" "Reading Plan Entry" entry
             (file "~/Gdrive/org/reading-plan.org")
             (file "~/Gdrive/org/templates/reading-plan-tmpl.txt")
             :empty-lines 1)
            ("s" "Scheduled ToDos")
            ("sd" "Scheduled ToDo - Date Only" entry
             (file+headline "~/Gdrive/org/daily.org" "New ToDos")
             (file "~/Gdrive/org/templates/todo-scheduled-tmpl.txt"))
            ("st" "Scheduled ToDo - Date and Time" entry
             (file+headline "~/Gdrive/org/daily.org" "New ToDos")
             (file "~/Gdrive/org/templates/todo-scheduled-time-tmpl.txt"))
            ("sc" "Google Calendar Entry" entry
             (file "~/Gdrive/org/calendar.org")
             (file "~/Gdrive/org/templates/cal-org-tmpl.txt"))))
    (message "org files path set for linux.")
    ))
 ((eq system-type 'windows-nt )
  (progn
    (setq org-capture-templates
          '(("b" "New Book" entry
             (file+headline "g:/My Drive/org/books/books.org" "New Items")
             (file "g:/My Drive/org/books/books-tmpl.txt")
             :empty-lines-after 1)
            ("t" "New ToDo Item (Unscheduled)" entry
             (file+headline "g:/My Drive/org/daily.org" "Captures")
             (file "g:/My Drive/org/templates/todo-tmpl.txt")
             :empty-lines-after 1)
            ("n" "New Note" entry
             (file "g:/My Drive/org/notes.org")
             (file "g:/My Drive/org/templates/note-tmpl.txt")
             :empty-lines-after 1)
            ("p" "Plain Text Note" plain
             (file "g:/My Drive/org/notes.org")
             (file "g:/My Drive/org/templates/note-plain-tmpl.txt")
             :empty-lines-before 1 :empty-lines-after 1)
            ("j" "Journal Entry" entry
             (file+olp+datetree "g:/My Drive/org/journal.org")
             (file "g:/My Drive/org/templates/journal-tmpl.txt"))
            ("r" "Reading Plan Entry" entry
             (file "g:/My Drive/org/reading-plan.org")
             (file "g:/My Drive/org/templates/reading-plan-tmpl.txt")
             :empty-lines 1)
            ("s" "Scheduled ToDos")
            ("sd" "Scheduled ToDo - Date Only" entry
             (file+headline "g:/My Drive/org/daily.org" "New ToDos")
             (file "g:/My Drive/org/templates/todo-scheduled-tmpl.txt"))
            ("st" "Scheduled ToDo - Date and Time" entry
             (file+headline "g:/My Drive/org/daily.org" "New ToDos")
             (file "g:/My Drive/org/templates/todo-scheduled-time-tmpl.txt"))
            ("sc" "Google Calendar Entry" entry
             (file "g:/My Drive/org/calendar.org")
             (file "g:/My Drive/org/templates/cal-org-tmpl.txt"))))
    (message "org files path set for windows."))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;		updated completion list for TODO items
;;		      Thu Oct 27, 2011  6:36 AM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq org-todo-keywords
      '((sequence "TODO(t)" "INPROGRESS(i)" "|" "DONE(d@)" "DISCARDED(c!)")))

;; Custom faces for TODO items
;; Sat Sep 18, 2021  9:44 AM
(setq org-todo-keyword-faces
      '(("TODO" . org-warning)
        ("INPROGRESS" . (:foreground "yellow" :weight normal))
        ("DISCARDED" . (:foreground "blue" :weight bold))
        ("DONE" . "green")
        ("TOREAD" . org-warning)
        ("STARTED" . (:foreground "yellow" :weight normal))
        ("PAUSED" . "cornflower blue")
        ("FINISHED" . (:foreground "DarkTurquoise" :weight bold))
        ))
;;
;; book management system in org
;; Sat Jul 31, 2021  6:10 AM
(use-package org-books :defer t :config (setq org-books-file org-books-file-windows))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;		 Org integration with Google Calendar
;;		      Wed Aug  4, 2021 11:12 PM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package org-gcal :defer t)
;;
;; set up calendar auth tokens
;; calendar file location by OS
;;
(cond
 ((eq system-type 'gnu/linux)
  (progn
    (setq org-gcal-client-id "288869755866-66e0sj8pv9kq6s63rbde41mc5vtc80lp.apps.googleusercontent.com"
          org-gcal-client-secret "7gKqn_YsUbGoJXzSlacgTUIP"
          org-gcal-fetch-file-alist '(("powem@ctpowe.net" .  "~/Gdrive/org/calendar.org")))))
 ((eq system-type 'windows-nt)
 (progn
   (setq org-gcal-client-id "288869755866-66e0sj8pv9kq6s63rbde41mc5vtc80lp.apps.googleusercontent.com"
         org-gcal-client-secret "7gKqn_YsUbGoJXzSlacgTUIP"
         org-gcal-fetch-file-alist '(("powem@ctpowe.net" .  "g:/My Drive/org/calendar.org"))))))

;; HTML to ORG
;; Wed Oct 20, 2021  5:39 PM
(use-package html2org :defer t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;		     org-roam note-taking system                      ;;
;;		      Wed Aug 11, 2021  7:50 PM                       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package org-roam :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "g:/My Drive/org/org-roam")
  (org-roam-completion-everywhere t)
  (org-roam-capture-templates
   '(("d" "default" plain
      "%?"
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
      :unnarrowed t)
   ("l" "programming language" plain
      "* Characteristics\n\n- Family: %?\n- Inspired by: \n\n* Reference:\n\n"
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
      :unnarrowed t)
   ("b" "book notes" plain
    "\n* Source\n\nAuthor: %^{Author}\nTitle: ${title}\nYear: %^{Year}\n\n* Summary\n\n%?"
    :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
    :unnarrowed t)
   ("s" "studies" entry
    "\n* %^{Topic:|History|Reading|Programming|Data Science|Writing|Web}\n\n** %^{Source:|Book|Video|Podcast|News|Radio|Magazine}\n\n*** Notes\n\n%?"
    :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
    :unnarrowed t)
   ))
  :bind
  (("C-c r l" . org-roam-buffer-toggle)
   ("C-c r f" . org-roam-node-find)
   ("C-c r i" . org-roam-node-insert)
   :map org-mode-map
   ("C-M-i" . completion-at-point))
  :config
  (org-roam-db-autosync-mode))
(setq org-roam-v2-ack t)
(use-package org-zettelkasten :defer t)
;; fancy list icons
(use-package org-superstar :defer t
  :hook (org-mode . (lambda() (org-superstar-mode 1)))
  )
;; setup to control init files in org
(use-package org-dotemacs :defer t)
;;
;; rename priorities to get rid of the A-B-Cs
;; Thu Oct 14, 2021  4:40 PM
(use-package org-fancy-priorities :defer t
  :hook (org-mode . org-fancy-priorities-mode)
  :config (setq org-fancy-priorities-list '((?A . "High") (?B . "Medium") (?C . "Low")))
  )
;; journaling package - creates a journal in a separate directory
;; Thu Oct 14, 2021  4:21 PM
(use-package org-journal :defer t)
(bind-key "C-c jj" 'org-journal-new-entry)
(bind-key "C-c js" 'org-journal-search)
(bind-key "C-c jf" 'org-journal-open-next-entry)
(bind-key "C-c jb" 'org-journal-open-previous-entry)
;; date format `Thu 14 Oct 2021'
;; Thu Oct 14, 2021  4:37 PM
(setq org-journal-date-format "%a %d %b %Y")

;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;			     end org mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; automatically tangle files on save
;; set file header #+auto_tangle: t
;; Thu Oct 14, 2021 10:44 AM
(use-package org-auto-tangle :defer t
  :hook (org-mode . org-auto-tangle-mode))

;; REPL-based eval of code blocks
;; Tue Oct 19, 2021  7:19 PM
;;
(use-package org-babel-eval-in-repl :defer t)
;;
;; pandoc to convert one markdown to another
;; Wed Oct 20, 2021  6:03 PM
(use-package pandoc-mode :defer t)

;; REPL support for JS in `org-mode' src blocks
;; Tue Oct 19, 2021  7:14 PM
;; NB This isn't working correctly
;; `org-babel' now has JS support built in.
(with-eval-after-load "ob"
  (require 'org-babel-eval-in-repl)
  (define-key org-mode-map (kbd "C-c oo") 'ober-eval-in-repl)
  (define-key org-mode-map (kbd "M-C-<return>") 'ober-eval-block-in-repl))

;; set `org-babel-load-languages' manually just add any new languages

(defvar my-ob-compile-progs nil)
;; example - add new cons cells as needed
;; (setq progs '((awk . t) (emacs-lisp . t)))
;; prolly don't need to do this for one item but for multiple items it
;; will be handy. also, now won't forget how to do it. ;-)
;; eval the union to create a new `org-babel-load-languages' and put
;; the new list in place
;; `-union' discards duplicates.
;; (setq org-babel-load-languages (-union org-babel-load-languages my-ob-compile-progs))

(setq org-babel-load-languages '((prolog . t) (rust . t)
                                 (fsharp . t) (go . t) (typescript . t) (awk . t)
                                 (emacs-lisp . t) (octave . t) (ruby . t) (lua . t)))

(setq more-languages '((prolog . t) (rust . t)
                                 (fsharp . t) (go . t) (typescript . t) (awk . t)
                                 (emacs-lisp . t) (octave . t) (ruby . t) (lua . t)
                                 (fortran . t) (makefile . t) (scheme . t ) (sml . t)
                                 (ocaml . t) (shell . t) (mermaid . t) ))

(org-babel-do-load-languages 'org-babel-load-languages more-languages)

;;; Javascript support
;; (require 'js3-mode)  ; if not done elsewhere
;; (require 'js2-mode)  ; if not done elsewhere
;; (require 'js-comint) ; if not done elsewhere
;; (with-eval-after-load 'js3-mode
;;   (require 'eval-in-repl-javascript)
;;   (define-key js3-mode-map (kbd "<C-return>") 'eir-eval-in-javascript))

;; (with-eval-after-load 'js2-mode
;;   (require 'eval-in-repl-javascript)
;;   (define-key js2-mode-map (kbd "<C-return>") 'eir-eval-in-javascript))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			Miscellaneous Settings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'recentf)
(recentf-mode 1)

(with-suppressed-warnings ((free-var display-time-24hr-format default-major-mode)))

(setq display-time-24hr-format t)
(setq default-major-mode 'text-mode
      text-mode-hook 'turn-on-auto-fill
      fill-column 72)


;; turn on line numbers in all buffers
;; Sun Oct 17, 2021  9:46 PM
(global-display-line-numbers-mode)
;;
;; add tempo interactive functions
;; Sun Mar 16, 2003  7:51 PM
;;
;; Wed Jul 14, 2021  1:24 PM
;; This is for making templates like this
;; (tempo-define-template "c-include"
;; 		       '("#include <" r ".h>" > n
;; 			 )
;; 		       "#include"
;; 		       "Insert a #include <> statement"
;; 		       'c-tempo-tags)
;;
;; (defvar c-tempo-tags nil
;;   "Tempo tags for C mode")
;; (tempo-use-tag-list 'c-tempo-tags)
;;
;; call M-x `tempo-template-c-include'
;; it inserts `#include <.h>' with point before the .
;; like this:
;; #include <`enter-include-file-name-here'.h>
;; probably superceded by snippets

(require 'tempo)
(setq tempo-interactive t)
(autoload 'align "align" nil t)

;; whitespace mode
;; Sun Sep  5, 2021  9:39 AM
(global-whitespace-mode t)
;; set these after mode is turned on, keep from getting the 'free
;; variable' error
(with-eval-after-load "whitespace"
  (setq whitespace-style '(face trailing newline lines-tail empty))
  (setq whitespace-line-column 80)
  )
;; focus mode dims surrounding text of what is being worked on.
;; Sun Sep  5, 2021  9:58 AM
;; package deleted Sat Oct 30, 2021  8:16 PM
;; (use-package focus :defer t)
;; (focus-mode t)
;;
;; Company mode global installation
;; Wed Dec 30, 2020  7:07 PM
(with-eval-after-load "company" (global-company-mode))

;; turn on electric pairs (), "" and the like, globally
;; Tue Jun 22, 2021  6:42 PM
(electric-pair-mode)

;; electric operator mode, puts spaces between operators
;; Wed Jun 23, 2021 10:27 PM
(use-package electric-operator
  :ensure t
  :hook (rust-mode . electric-operator-mode)
  (js2-mode . electric-operator-mode)
  (lisp-mode . electric-operator-mode)
  )
;;
;; expands the info given by help commands
;; Fri Jul 16, 2021  1:58 PM
;; (use-package helpful :defer t
;;   :bind (:map global-map
;;               ("C-h f"   . #'helpful-callable)
;;               ("C-h v"   . #'helpful-variable)
;;               ("C-h k"   . #'helpful-key)
;;               ("C-c C-d" . #'helpful-at-point)
;;               ("C-h F"   . #'helpful-function)
;;               ("C-h C"   . #'helpful-command)))

;; ;; this fixes an issue with helpful in version 29+
;; (when (not(version<= emacs-version "28"))
;; ;;  REVIEW See Wilfred/elisp-refs#35. Remove once fixed upstream.
;; (defvar read-symbol-positions-list nil))

;; ;; (if (fboundp 'helpful-callable)
;; ;;     (progn
;; ;;       (global-set-key (kbd "C-h f") #'helpful-callable)
;; ;;       (global-set-key (kbd "C-h v") #'helpful-variable)
;; ;;       (global-set-key (kbd "C-h k") #'helpful-key)
;; ;;       (global-set-key (kbd "C-c C-d") #'helpful-at-point)
;; ;;       (global-set-key (kbd "C-h F") #'helpful-function)
;; ;;       (global-set-key (kbd "C-h C") #'helpful-command)))

;; ;; icons for everything
;; ;; Wed Aug 18, 2021  9:09 PM
;; (use-package all-the-icons :defer t)
;; ;; for Dired
;; Thu Oct 14, 2021 11:42 AM
(use-package all-the-icons-dired :defer t)
;;
;; additional control of faces
;; Wed Jun 23, 2021 11:37 PM
;; (use-package face-explorer :defer t)
;;
;; expand imenu usage
;; Sat Jun 26, 2021  2:25 AM
(use-package imenu-anywhere :defer t
  :config (global-set-key (kbd "C-.") #'imenu-anywhere))
;;
;; show imenu in a popup window
;; Sat Jun 26, 2021  2:26 AM
(use-package popup-imenu :defer t)
;;
(use-package tramp :defer t :config (setq tramp-default-method "ssh"))
;;
;; IRC client
;; Mon Aug  9, 2021  9:20 PM
(use-package circe :defer t)
;;
;; text folding
;; Mon Jun 28, 2021  1:26 PM
;; removed until I feel like configuring it Sat Oct 16, 2021  2:40 PM
(use-package folding :config (folding-add-to-marks-list 'powershell-mode "#region" "#endregion"))
;; on-the-fly linting set globally
;; Sat Aug 28, 2021 10:19 PM
;; flycheck removed Thu Oct 14, 2021 10:09 AM
(use-package flycheck :config (add-hook 'after-init-hook #'global-flycheck-mode))

(global-font-lock-mode 1)
;; added Mon Oct 24, 2011  5:07 PM
(show-paren-mode 1)
(add-hook 'write-file-functions 'time-stamp)
;;
;; Deploying the 'f' interface for file handling
;; Wed Dec 30, 2020  8:50 PM
(use-package f :defer t)
;; Using Desktop to save config information
;; Sun Jan 31, 2021 10:43 AM
(desktop-save-mode t)
;; Add functionality to dired, including sort options
;; Sun Aug 29, 2021  2:08 PM
(use-package diredc :defer t)
;;
;; Clippy the paperclip for popup help
;; Sat Oct 16, 2021  2:54 PM
(use-package clippy :defer t)
;;
;; fancify eshell
;; Tue Oct 19, 2021  2:14 PM
(use-package eshell-git-prompt :defer t)
;;
;; open/close the shell at the stroke of a key
;; Tue Oct 19, 2021  2:16 PM
(use-package eshell-toggle :defer t)
(global-set-key (kbd "C-c s") 'eshell-toggle)
;;
;; multiple cursors
;; Wed Oct 20, 2021  6:54 PM
(use-package multiple-cursors :defer t)
;;
;; PDF Tools
;; Fri Feb 25, 2022 11:13 PM
(pdf-tools-install)
;; clean up the modeline display
;; Fri Oct 22, 2021  9:55 AM
;; I decided I want a complicated modeline
;; (use-package simple-modeline :defer t)
;;
;; enter alt codes with `META' key
;; Tue Mar  1, 2022 10:17 AM
(use-package alt-codes :defer t)
(global-alt-codes-mode t)
(global-set-key (kbd "C-c mc") 'alt-codes-insert)

;;
;; calibredb library interface
;; Tue Mar  1, 2022 10:18 AM
(use-package calibredb :defer t
  :config
  (setq calibredb-root-dir "c:/Users/micha/Calibre Library")
  (setq calibredb-db-dir (expand-file-name "metadata.db" calibredb-root-dir))
  (setq calibredb-library-alist '(("c:/Users/micha/Calibre Library"))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;		      end Miscellaneous Settings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;		    begin status line gingerbread
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(display-time)
(setq column-number-mode t)
;; This records keystrokes - every single one. Along with mouse
;; events. It's overkill.
;; Sun Sep 19, 2021  8:35 AM
;;
;; package deleted Sat Oct 30, 2021  8:19 PM
;; (use-package keycast :defer t)
;; (keycast-log-mode)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;		     end status line gingerbread
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			     begin macros
;;		      Tue Aug 24, 2021  5:15 PM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(fset 'r-equal-sign
   (kmacro-lambda-form [?< ?-] 0 "%d"))
(fset 'r-dplyr-pipe
   "%>%")
(fset 'r-in-symbol "%in%")

(bind-key "C-c C-n" 'r-in-symbol)
(bind-key "C-c <" 'r-equal-sign)
(bind-key "C-c >" 'r-dplyr-pipe)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			      end macros
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			begin helper functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; F2
(defun edit-emacs()
  "Load the `.emacs' init file for editing or switch to the buffer if it already is loaded."
  (interactive)
  (if (not (eq nil (get-buffer ".emacs")))
          (switch-to-buffer ".emacs")
        (find-file dotemacs-file-name)))
;; F11
(defun get-daily-todo-list ()
  "Load my `org-mode' daily todo list."
  (interactive)
  (if (not (eq nil (get-buffer "daily.org")))
          (switch-to-buffer "daily.org")
        (find-file "c:/Users/micha/OneDrive/org/daily.org")))
;; F3
(defun insert-date()
  "Insert date at point."
  (interactive)
  (insert (format-time-string "%a %b %e, %Y %l:%M %p")))

;; a function to bind byte-compiling init files to a key. Makes it
;; unnecessary to do it through a source block in org.
;; Thu Oct 14, 2021  6:14 PM
;; keybindings:
;; - C-c iv variables file
;; - C-c im main config file
;; - C-c ip prog modes file
(defun compile-init-file(file)
  "Compile or recompile Emacs init files after updating. FILE is the variable holding the name of the particular file."
  (interactive "f")
  (if (file-exists-p file)
      (byte-compile-file file)
    (message file)
    )
  )

;; Delete a property from an org document. To be used to get rid of
;; unwanted nodes in `org-roam'. From reddit r/orgmode.
;; Sat Oct 16, 2021 10:17 AM
(defun org-id-delete-entry ()
  "Remove/delete an ID entry. Saves the current point and only
does this if inside an org-heading."
  (interactive)
  (save-excursion
    (org-back-to-heading t)
    (when (org-entry-delete (point) "ID"))))

;; Upgrade packages automatically, without using Package interface.
;; from `https://emacs.stackexchange.com/questions/16398/noninteractively-upgrade-all-packages'
;; Tue Nov  9, 2021  8:00 AM
(defun package-upgrade-all ()
  "Upgrade all packages automatically without showing *Packages* buffer."
  (interactive)
  (package-refresh-contents)
  (let (upgrades)
    (cl-flet ((get-version (name where)
                (let ((pkg (cadr (assq name where))))
                  (when pkg
                    (package-desc-version pkg)))))
      (dolist (package (mapcar #'car package-alist))
        (let ((in-archive (get-version package package-archive-contents)))
          (when (and in-archive
                     (version-list-< (get-version package package-alist)
                                     in-archive))
            (push (cadr (assq package package-archive-contents))
                  upgrades)))))
    (if upgrades
        (when (yes-or-no-p
               (message "Upgrade %d package%s (%s)? "
                        (length upgrades)
                        (if (= (length upgrades) 1) "" "s")
                        (mapconcat #'package-desc-full-name upgrades ", ")))
          (save-window-excursion
            (dolist (package-desc upgrades)
              (let ((old-package (cadr (assq (package-desc-name package-desc)
                                             package-alist))))
                (package-install package-desc)
                (package-delete  old-package)))))
      (message "All packages are up to date"))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			 end helper functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;			    Font Settings
;;;		      Thu Sep  9, 2021  8:19 PM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Use variable width font faces in current buffer
(defun my-buffer-face-mode-variable ()
  "Set font to a variable width (proportional) fonts in current
buffer, with a height of 11pt."
  (interactive)
  (setq buffer-face-mode-face '(:family "Roboto" :height 110))
  (buffer-face-mode))

;; Use monospaced font faces in current buffer
(defun my-buffer-face-mode-fixed ()
  "Set a fixed width (monospace) font in current buffer, with a
height of 10pt."
  (interactive)
  (setq buffer-face-mode-face '(:family "Cascadia Mono PL" :height 100))
  (buffer-face-mode))

;; font face for org-mode
(defun my-buffer-face-org-mode ()
  "Set a fixed width (monospace) font in `org-mode' buffers, with a
height of 11pt."
  (interactive)
  (setq buffer-face-mode-face '(:family "Fira Mono" :height 110))
  (buffer-face-mode))

;; font face for text-mode
(defun my-buffer-face-text-mode ()
  "Set a variable width font in `text-mode' buffers, with a height of 12pt."
  (interactive)
  (setq buffer-face-mode-face '(:family "IBM Plex Serif" :height 120 :weight medium))
  (buffer-face-mode))

;; apparently, `nxml-mode' inherits from `text-mode', so set this specially
(defun my-buffer-face-xml-mode ()
  "Set a fixed width (monospace) font in `nxml-mode' buffers, with
a height of 11pt."
  (interactive)
  (setq buffer-face-mode-face '(:family "Fira Code" :height 110 :weight medium))
  (buffer-face-mode))

;; how to use them
 ;; Set default font faces for some modes
(add-hook 'Info-mode-hook 'my-buffer-face-mode-variable)
(add-hook 'emacs-lisp-mode-hook 'my-buffer-face-mode-fixed)
;; `vue-mode' inherits from `html-mode', which is text-based
;; Thu Feb 24, 2022  5:17 PM
(add-hook 'vue-mode-hook 'my-buffer-face-mode-fixed)
;; this sets the default font for every programming mode that derives
;; from `prog-mode', which is likely most.
(add-hook 'prog-mode-hook 'my-buffer-face-mode-fixed)
(add-hook 'org-mode-hook 'my-buffer-face-org-mode)
(add-hook 'text-mode-hook 'my-buffer-face-text-mode)
(add-hook 'nxml-mode-hook 'my-buffer-face-xml-mode)
;; for eshell
(add-hook 'eshell-mode-hook 'my-buffer-face-mode-fixed)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Set up an abbrev definition file. Create it if it doesn't exist.
;; Sun Jan 31, 2021 11:09 AM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun set-up-abbrev-file()
    "Set up the use of the abbrev file.
This gets around the problem of an abbrev file not existing when
starting up a new installation."
    (setq-default abbrev-mode t)
    (read-abbrev-file "~/.emacs.d/abbrev_defs")
    (setq save-abbrevs 'silently)
    (message "abbrev file setup completed"))

(cond ((not (file-exists-p "~/.emacs.d/abbrev_defs"))
       (with-temp-buffer (write-file "~/.emacs.d/abbrev_defs"))
       (set-up-abbrev-file))
      ((file-exists-p "~/.emacs.d/abbrev_defs")
       (set-up-abbrev-file))
      (t (message "Problem loading abbrev file.")))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			end abbrev file setup
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Emacs Multimedia System
;; Sun Nov 14, 2021  3:34 PM
(use-package emms :defer t)
(require 'emms-setup)
(emms-all)
(require 'emms-player-mplayer)
(setq emms-source-file-default-directory "c:/Users/micha/OneDrive/Music/")

;; try to pass the `-tt' option to ssh to override pseudo-terminal issue
;; Tue Nov  2, 2021 12:45 PM
(with-eval-after-load 'tramp
  (add-to-list 'tramp-methods
               '("sshecs"
                 (tramp-login-program "ssh")
                 (tramp-login-args
                  (("-l" "%u")
                   ("-tt")
                   ("-p" "%p")
                   ("%c")
                   ("-e" "none")
                   ("%h")))
                 (tramp-async-args
                  (("-q")))
                 (tramp-direct-async t)
                 (tramp-remote-shell "/bin/sh")
                 (tramp-remote-shell-login
                  ("-l"))
                 (tramp-remote-shell-args
                  ("-c"))))
  (tramp-set-completion-function "sshecs" tramp-completion-function-alist-ssh))

;; zettelkasten note system
(use-package zettelkasten :defer t
  :config (zettelkasten-mode t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			      start helm
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; some keybindings for helm
;; C-x c M-y helm-show-kill-ring
;; C-x c C-x r b helm-source-filtered-bookmarks <- shows all bookmarks from all files
;; C-x C-f helm-find-files
;; C-x b helm-buffers-list
;; C-x c M-x helm-M-x
;; C-x c C-c SPC helm-all-mark-rings
;; C-x c @, helm-list-elisp-packages
;; C-x c M-s o helm-occur
;; Mon Jul  5, 2021 11:50 AM
;;
(use-package helm :defer t
  :config
  (helm-mode)
  (setq completion-styles '(flex))
  (helm-popup-tip-mode)
  (add-to-list 'helm-completing-read-handlers-alist '(org-capture . helm-org-completing-read-tags))
  (add-to-list 'helm-completing-read-handlers-alist '(org-set-tags . helm-org-completing-read-tags))
  :bind (("C-x C-f" . helm-find-files)
         ("C-x b" . helm-buffers-list))
  ("C-x c M-y" . helm-show-kill-ring)
  ("C-x c C-x r b" . helm-source-filtered-bookmarks))
;;
;; Mon Jul  5, 2021 11:39 AM
(use-package helm-lsp :defer t)
;;
;; Fri Oct 15, 2021  4:29 PM
(use-package helm-company
  :defines company-active-map
  :defer t)
(eval-after-load 'company
  '(progn
     (define-key company-mode-map (kbd "C-:") 'helm-company)
     (define-key company-active-map (kbd "C-:") 'helm-company)))
;;
;; Thu Aug 12, 2021  9:06 AM
;; Not useful on Windows
;; (use-package helm-system-packages :defer t)

;; can't make this work with hunspell
;; Tue Jul 13, 2021  3:21 PM
;; (use-package flyspell-correct :after flyspell
;;   :bind (:map flyspell-mode-map ("C-;" . flyspell-correct-wrapper))
;;   :config (add-to-list 'ispell-hunspell-dictionary-alist "c:/tools/hunspell-v1.7.0/Hunspell/dict/en_US.aff")
;;   )

;; (use-package flyspell-correct-helm :after (flyspell-correct))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			       end helm
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package helm-spotify-plus :defer t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;			  begin keybindings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-set-key [f5] 'start-info-with-emacs)
;; (global-set-key [f12] 'perl-repl)
(global-set-key [f2] 'edit-emacs)
(global-set-key [f3] 'insert-date)
(global-set-key [f4] 'js-comint-repl)
(global-set-key [f6] 'package-list-packages)
(global-set-key [f7] 'start-clisp)
(global-set-key [f8] 'js-comint-repl)
(global-set-key [f9] 'kill-active-lisp-buffer)
(global-set-key [f11] 'get-daily-todo-list)
(global-set-key "\C-cc" 'comment-region)
(global-set-key "\C-cu" 'uncomment-region)
(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key "\M-o\M-s" 'center-line)
(global-set-key "\C-cw" 'whitespace-cleanup)
;; compile init files on command
;; Use after tangling org files
;; Thu Oct 14, 2021  6:15 PM
(bind-key "C-c iv" (lambda()(interactive) (compile-init-file config-vars-source)))
(bind-key "C-c im" (lambda()(interactive) (compile-init-file config-main-source)))
(bind-key "C-c ip" (lambda()(interactive) (compile-init-file prog-modes-source)))
(bind-key "C-c i C-p" (lambda()(interactive) (native-compile prog-modes-source)))
(bind-key "C-c i C-m" (lambda()(interactive) (native-compile config-main-source)))
(bind-key "C-c i C-v" (lambda()(interactive) (native-compile config-vars-source)))

(which-key-add-key-based-replacements "C-c iv" "variables file")
(which-key-add-key-based-replacements "C-c ip" "prog modes")
(which-key-add-key-based-replacements "C-c im" "main config")

(which-key-add-key-based-replacements "C-c i C-m" "native compile main")
(which-key-add-key-based-replacements "C-c i C-p" "native compile prog modes")
(which-key-add-key-based-replacements "C-c i C-v" "native compile vars")
;;
;; EMMS bindings
;; Sun Nov 14, 2021  8:00 PM
(bind-key "C-c er" 'emms-random)    ;; randomly select track from playlist
(bind-key "C-c ef" 'emms-play-file) ;; play a given file
(bind-key "C-c ea" 'emms-add-file)  ;; add a given file to library
(bind-key "C-c es" 'emms-start)     ;; start playing
(bind-key "C-c et" 'emms-stop)      ;; stop playing
(bind-key "C-c en" 'emms-next)      ;; play next track
(bind-key "C-c ep" 'emms-previous)  ;; play previous track
(bind-key "C-c eh" 'emms-shuffle)   ;; shuffle playlist
(bind-key "C-c ew" 'emms-show)      ;; show track information
(bind-key "C-c eb" 'emms-browser)   ;; launch emms browser

(which-key-add-key-based-replacements "C-c er" "EMMS play random track")
(which-key-add-key-based-replacements "C-c ef" "EMMS play a file")
(which-key-add-key-based-replacements "C-c ea" "EMMS add a file")
(which-key-add-key-based-replacements "C-c es" "EMMS start playing")
(which-key-add-key-based-replacements "C-c et" "EMMS stop playing")
(which-key-add-key-based-replacements "C-c en" "EMMS next track")
(which-key-add-key-based-replacements "C-c ep" "EMMS previous track")
(which-key-add-key-based-replacements "C-c eh" "EMMS shuffle playlist")
(which-key-add-key-based-replacements "C-c ew" "EMMS show track info")
(which-key-add-key-based-replacements "C-c eb" "EMMS launch EMMS browser")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;			   end keybindings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			    begin treemacs
;;		      Wed Aug 11, 2021  8:22 PM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package treemacs :defer t
  :config
  (progn
    (setq treemacs-collapse-dirs                 (if treemacs-python-executable 3 0)
          treemacs-deferred-git-apply-delay      0.5
          treemacs-directory-name-transformer    #'identity
          treemacs-display-in-side-window        t
          treemacs-eldoc-display                 t
          treemacs-file-event-delay              5000
          treemacs-file-extension-regex          treemacs-last-period-regex-value
          treemacs-file-follow-delay             0.2
          treemacs-file-name-transformer         #'identity
          treemacs-follow-after-init             t
          treemacs-expand-after-init             t
          treemacs-git-command-pipe              ""
          treemacs-goto-tag-strategy             'refetch-index
          treemacs-indentation                   2
          treemacs-indentation-string            " "
          treemacs-is-never-other-window         nil
          treemacs-max-git-entries               5000
          treemacs-missing-project-action        'ask
          treemacs-move-forward-on-expand        nil
          treemacs-no-png-images                 nil
          treemacs-no-delete-other-windows       t
          treemacs-project-follow-cleanup        nil
          treemacs-persist-file                  (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-position                      'left
          treemacs-read-string-input             'from-child-frame
          treemacs-recenter-distance             0.1
          treemacs-recenter-after-file-follow    nil
          treemacs-recenter-after-tag-follow     nil
          treemacs-recenter-after-project-jump   'always
          treemacs-recenter-after-project-expand 'on-distance
          treemacs-litter-directories            '("/node_modules" "/.venv" "/.cask")
          treemacs-show-cursor                   nil
          treemacs-show-hidden-files             t
          treemacs-silent-filewatch              nil
          treemacs-silent-refresh                nil
          treemacs-sorting                       'alphabetic-asc
          treemacs-space-between-root-nodes      t
          treemacs-tag-follow-cleanup            t
          treemacs-tag-follow-delay              1.5
          treemacs-user-mode-line-format         nil
          treemacs-user-header-line-format       nil
          treemacs-width                         35
          treemacs-workspace-switch-cleanup      nil)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)

    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode 'always)
    (pcase (cons (not (null (executable-find "git")))
                 (not (null treemacs-python-executable)))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple))))
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))

(use-package treemacs-icons-dired :after (treemacs dired) :ensure t
  :config (treemacs-icons-dired-mode))

(use-package treemacs-magit :after (treemacs magit) :ensure t)
;; more icon gingerbread
(use-package treemacs-all-the-icons :defer t)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			     end treemacs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;   Create a menu for the extension functions to automate access
;;	    (Commentary updated) Thu Jul 24, 2014  7:24 AM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun add-extensions-menu ()
  "Add a menu name `Extensions' to the menu-bar.
Populate it with items I have created for my own use.  Call this
function at the end of the init process."
  (modify-frame-parameters (selected-frame)
                           '((menu-bar-lines . 2)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;	      Make a menu keymap (with a prompt string)
;;;	     and make it the menu bar item's definition.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (define-key-after global-map [menu-bar extensions]
        (cons "Extensions" (make-sparse-keymap "Extensions")))

  ;; list packages
  (define-key global-map
        [menu-bar extensions list-packages]
        '("List Packages" . package-list-packages))

  ;; f3
  ;; insert-date
  (define-key-after global-map
    [menu-bar extensions indate]
    '("Insert Date" . insert-date))

  ;; edit emacs config
  (define-key-after global-map
    [menu-bar extensions edit-emacs]
    '("Edit .emacs" . edit-emacs))

  ;; info - top dir
  (define-key-after global-map
    [menu-bar extensions info]
    '("Info" . start-info))

  ;;; emacs info
  (define-key-after global-map
    [menu-bar extensions emacs-info]
    '("Emacs Manual (info)" . start-info-with-emacs))

  ;; info - cc-mode
  (define-key-after global-map
    [menu-bar extensions ccmode-node]
    '("cc-mode Manual (info)" . start-info-with-ccmode))

  ;; info - org-mode
  (define-key-after global-map
    [menu-bar extensions org-node]
    '("org-mode Manual (info)" . start-info-with-org))

  ;; info - elisp manual
  (define-key-after global-map
    [menu-bar extensions org-node]
    '("Elisp Manual (info)" . start-info-with-elisp))

  ;; common lisp
  (define-key-after global-map
    [menu-bar extensions clisp]
    '("Start GNU Clisp" . start-clisp))

  ;; JavaScript (node)
  ;; f8
  ;; Wed Dec 30, 2020  9:39 PM
  (define-key-after global-map
    [menu-bar extensions js]
    '("Run JS REPL" . js-comint-repl))

  ;; kill lisp buffer
  ;; f9
  (define-key-after global-map
    [menu-bar extensions kill-lisp]
    '("Kill Lisp Buffer" . kill-active-lisp-buffer))

  ;; f11
  ;; daily to-do list
  (define-key-after global-map
    [menu-bar extensions get-daily]
    '("Get Daily ToDo List" . get-daily-todo-list))

  ) ;; end `add-extensions-menu'

(add-extensions-menu)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			  end extension menu
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;	Functions for calling *info* in special circumstances
;;		      Sun Jan 31, 2021 10:47 AM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun start-info ()
"Start the main info menu."
  (interactive)
  (if (not (eq nil (get-buffer "*info*")))
	  (kill-buffer "*info*"))
  (info))

;; Lisp Reference Manual
;; Fri Feb 26, 2021 10:39 PM
(defun start-info-with-elisp()
  "Load the elisp manual."
  (interactive)
  (if (not (eq nil (get-buffer "*info*")))
	  (kill-buffer "*info*"))
  (info "elisp"))

;; Lisp Reference Manual
;; Fri Feb 26, 2021 10:39 PM
(defun start-info-with-emacs()
  "Load the Emacs manual."
  (interactive)
  (if (not (eq nil (get-buffer "*info*")))
      (kill-buffer "*info*"))
  (info "emacs"))


(defun start-info-with-ccmode ()
"Load the info documentation for cc-mode."
  (interactive)
  (if (not (eq nil (get-buffer "*info*")))
	  (kill-buffer "*info*"))
  (info "ccmode"))

(defun start-info-with-org ()
"Load the info documentation for `org-mode'."
  (interactive)
  (if (not (eq nil (get-buffer "*info*")))
	  (kill-buffer "*info*"))
  (info "org"))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			       end info
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Sat Aug  7, 2021 12:56 PM
;; Customizations moved to file `custom.el'.
;;
;;--description="The JDEE Java Environment"
;;--info-file=./jdee.info
;;--info-dir=./dir
;;--name="JDEE"
;;install-info --info-file=./jdee.info --info-dir=./dir
;;(add-hook 'M-mode-hook 'enable-paredit-mode)

(provide '.emacs-config)
;;; .emacs-config.el ends here
