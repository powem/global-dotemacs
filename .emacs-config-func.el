;;; .emacs-config-func.el --- Miscellaneous functions for init -*- coding: utf-8 -*-
;;; Time-stamp: <2021-07-01 22:43:03 micha>
;;; Filename: .emacs-config-func.el
;;; This file contains local function definitions created for this installation
;;; of emacs
;;; Wed Sep  2, 2015  8:02 AM
;;;
;;;
;;; Various functions defined for my own purposes.
;;; Sun Oct 23, 2011  8:46 PM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; copied from Chassell's Introduction to Emacs Lisp
;;; Tue Dec 29, 2020 11:06 AM
;;;
;;; Commentary:
;;;
;;;
;;; Code:
;;;
;;; This require does work, despite the linting error from flycheck
(require '.emacs-config-vars)

(defun count-words-region (beginning end)
  "Print number of words in the region.
Argument BEGINNING General functions.
Argument END General functions."
  (interactive "r")
  (message "Counting words in region...")
  (save-excursion
	(let ((count 0))
	  (goto-char beginning)
	  (while (and (< (point) end)
				  (re-search-forward "\\w+\\W*" end t))
		(setq count (+ 1 count)))

	  (cond ((zerop count)
			 (message
			  "The region does NOT have any words."))
			((= 1 count)
			 (message
			  "The region has one word."))
			(t
			 (message
			  "The region as %d words." count))))))


;; (defun load-local-file (fname)
;;   "Attempt to load a specific lisp file from somewhere in the load-path, if it exists."
;;   (let ( (my-file (locate-file fname load-path)) )
;; 	(if (equal nil my-file)
;; 		(message "File %s not found: " fname)
;; 	  (progn
;; 		(message "File %s found: %s." fname my-file)
;; 		(load-file my-file)))))

;; (defun alt-load-local-file (fname)
;;   "Loads a specific lisp file from somewhere in the load-path. Removes a trailing slash, if necessary, to make all path values consistent. "
;;   (let ( (paths (append () load-path)) )
;; 	(catch 'break
;; 	  (while paths
;; 		(let ( (path (car paths)) (my-file nil) )
;; 		  (if (string-match "/$" path)
;; 			  (progn
;; 				(setq my-file (concat path fname))
;; 				(message "my-file is %s." my-file))
;; 			(progn
;; 			  (setq my-file (concat path "/" fname))
;; 			  (message "my-file is now %s" my-file)))
;; 		  (if (file-exists-p my-file)
;; 			  (progn
;; 				(setq paths ())
;; 				(load-file my-file)
;; 				(throw 'break 'true))
;; 			(progn
;; 			  (message "Removing %s from paths.  %d paths remain." (car paths) (length paths) )
;; 			  (setq paths (cdr paths)) ))
;; 		  )))))

;; (defun my-load-file (path fname)
;;   "Load a lisp file from the given, non-load-path location."
;;   (let ((my-file (concat path "/" fname)))
;; 	(if (file-exists-p my-file)
;; 		(and (message "file %s is loaded from %s" fname path) (load my-file))
;; 	  (message "file %s was not found and was not loaded." my-file)
;; 	  )))

;; (defun my-file-exists-p (fname)
;;   "Test for the existence of a file in load-path."
;;   (let ( (paths (append () load-path)) )
;; 	(catch 'break
;; 	  (while paths
;; 		(let ( (path (car paths)) (my-file nil) )
;; 		  (setq my-file (concat path "/" fname))
;; 		  (if (string-match "//" my-file)
;; 			  (and (setq my-file (replace-regexp-in-string "//" "/" my-file)) (message "fixed %s" my-file)))
;; 		  (if (file-exists-p my-file)
;; 			  (progn
;; 				(setq paths ())
;; 				(throw 'break 'true))
;; 			(setq paths (cdr paths)))
;;		  ))))

(defun edit-emacs()
  "Load the .emacs init file for editing or switch to the buffer if it already is loaded."
  (interactive)
  (if (not (eq nil (get-buffer ".emacs")))
	  (switch-to-buffer ".emacs")
	(find-file dotemacs-file-name)))

(defun get-daily-todo-list ()
  "Load my org-mode daily todo list."
  (interactive)
  (if (not (eq nil (get-buffer "daily.org")))
	  (switch-to-buffer "daily.org")
	(find-file "c:/Users/micha/OneDrive/org/daily.org")))

;; Wed Jun 26, 2013  8:30 AM
(defun test-dotemacs()
  "Compare the CVS and production .emacs files.  Return true if they are the same, false otherwise."
  (if (file-newer-than-file-p cvs-dotemacs-file-name dotemacs-file-name )
	  'true
	nil)
  )

;; Tue Oct 16, 2012  4:09 PM
;; This copies the CVS copy of the file into place when it is saved, if
;; the CVS copy is newer.
;; (defun update-dotemacs()
;;   "Update the .emacs if the CVS version is newer.  Run from after-save-hook only if actually editing the .emacs file."
;;   (interactive)
;;   (if (test-dotemacs)
;; 	  (copy-file cvs-dotemacs-file-name dotemacs-file-name t )
;; 	(message ".emacs file not copied."))
;;   )

;; Wed Jun 26, 2013  9:01 AM
;; Added to differentiate between updating at will and updating on save.

;; (defun update-dotemacs-hook()
;;   "Update the .emacs file while in the buffer.  To be attached to the save hook."
;;   (interactive)
;;   (if (and (string-equal cvs-dotemacs-file-name buffer-file-name) (test-dotemacs))
;; 	  (copy-file cvs-dotemacs-file-name dotemacs-file-name t )
;; 	(message ".emacs file not copied."))

;;   )

;; Mon Oct 15, 2012 10:09 AM
;; (defun turn-on-paredit () (paredit-mode 1))

;; My function to insert the date and time at point.
;;
(defun insert-date()
  "Insert date at point."
  (interactive)
  (insert (format-time-string "%a %b %e, %Y %l:%M %p")))

(provide '.emacs-config-func)

;;; .emacs-config-func.el ends here
