;;; -*- coding: utf-8 -*-
;;; custom.el --- Settings through the `Customize' interface.
;;;
;;; Commentary:
;;; These settings were moved into a separate file to cut
;;; down on number of changes made to `.emacs-config.el'.
;;;
;;; Code:
;;;
;; Customize
;; ===========================================================
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-term-color-vector
   [unspecified "#2d2a2e" "#ff6188" "#a9dc76" "#ffd866" "#78dce8" "#ab9df2" "#a1efe4" "#fcfcfa"] t)
 '(backward-delete-char-untabify-method 'hungry)
 '(bookmark-default-file "~/.emacs.d/.emacs-config/bookmarks")
 '(buffer-face-mode-face '(:family "Cascadia Code" :height 100))
 '(calibredb-format-all-the-icons t)
 '(calibredb-program "calibredb")
 '(calibredb-root-dir "c:/Users/micha/Calibre Library")
 '(clang-format-executable "C:/Program Files/LLVM/bin/clang-format.exe")
 '(clang-format-fallback-style "GNU")
 '(clang-format-style "GNU")
 '(column-number-mode t)
 '(company-clang-executable "C:/Program Files/LLVM/bin/clang.exe")
 '(company-idle-delay 0.2)
 '(compilation-message-face 'default)
 '(cua-global-mark-cursor-color "#7ec98f")
 '(cua-normal-cursor-color "#8d8b86")
 '(cua-overwrite-cursor-color "#e5c06d")
 '(cua-read-only-cursor-color "#8ac6f2")
 '(current-language-environment "UTF-8")
 '(custom-enabled-themes '(wheat))
 '(custom-safe-themes
   '("6a2cc07c407e8321f0df155988e60b39fe6f2488d2c79a6b14211854ea6fbc52" "317a45f190eaa3ccf8af6168aa89112d9cb794f87f409bc7a0638edee20d07fd" "06e0662b31a2ae8da5c6b5e9a05b25fabd1dc8dd3c3661ac194201131cafb080" "701b4b4e7989329a0704b92fc17e6600cc18f9df4f2466617ec91c932b5477eb" "ec38d891ab6805f112b47c18149190a50bf1e2967b6e9fc84453989b6f67b2f6" "d422c7673d74d1e093397288d2e02c799340c5dabf70e87558b8e8faa3f83a6c" "499c3dd62c262b0bbb3ea0f5c83e92db5eac4a2a58468b51900e0ca706a7ad12" "271d9389c0256fa7df2bbb6b5fd304d08bd6178371f46159d963621669e8c203" "be2c1a78f42783eab9ff068c3f09e81a7908a77a1d288ce8d704491165ef448b" "7c83927617afdf71cc90801ffee5965939505b7cfecdcbe15961fc32a0d9bd02" "be0efbaebc85494f3c1c06e320fd13a24abf485d5f221a90fe811cea9a39ed85" "9987adb99037db4fca049c26ab751370b37b7a84c458d8f11f6590d5a02e97d5" "e6e07c74bee845f48da5f46843a241d71d4ee3bf945fb506c05a08e5c872df3e" "ef92efb0f9178b4a1130ffae1571d71983d123f84acd6565e09cb58ca71982c9" "6e03b7f86fcca5ce4e63cda5cd0da592973e30b5c5edf198eddf51db7a12b832" "9df3ad3ca8a700784a2fbcfcdaca5da3b6149da52212f22d3a10f48da88581c3" "294c4b6a955149c93945f901a284140df29963a57939e9ed2fc4ad79b3605080" "8e5f723fc78532cfe9d76fd804dcd05ed5ea1eeea65507e25723b9e5934b1cbd" "2eb6dd78a83cdecc2ea783a2640f4c316f41e223353d4a32fe936113e8a2ddbf" "6c0d748fb584ec4c8a0a1c05ce1ae8cde46faff5587e6de1cc59d3fc6618e164" "d86ddb4126c19487edd10a0f1a16f77fdaadafbdac9f4fcd695cf1c0a215117b" "1a094b79734450a146b0c43afb6c669045d7a8a5c28bc0210aba28d36f85d86f" "0c5204945ca5cdf119390fe7f0b375e8d921e92076b416f6615bbe1bd5d80c88" "39a854967792547c704cbff8ad4f97429f77dfcf7b3b4d2a62679ecd34b608da" "1fbd63256477789327fe429bd318fb90a8a42e5f2756dd1a94805fc810ae1b62" "e8830baf7d8757f15d9d02f9f91e0a9c4732f63c3f7f16439cc4fb42a1f2aa06" "1f6039038366e50129643d6a0dc67d1c34c70cdbe998e8c30dc4c6889ea7e3db" "e04dbe0f0986679148845c8e893dfab392e6bc2a32dbd31f8ec820f3f0131049" "a4d393ee5beec12ca05d99dc4314bb96c9955ff4dfdef7eda414150ef4cda8c1" "5516001c13a43f1f5be2d7df8035445f8d7c73179f637e40c1503afb184d98f2" "09feeb867d1ca5c1a33050d857ad6a5d62ad888f4b9136ec42002d6cdf310235" "0ea5731605469a81203311ad7b4b3db03d6d7249ce621e3f2793ae3613d165ed" "b71da830ae97a9b70d14348781494b6c1099dbbb9b1f51494c3dfa5097729736" "aa9834b2ccf220220504aa4d0f6513ce9124a939c73586ad623565f7b3bbffb3" "42d116236094bbb32033705237877aa3e83d436649c92e541e941347f0a7e605" "cc2f32f5ee19cbd7c139fc821ec653804fcab5fcbf140723752156dc23cdb89f" "1048305cc1ac0f9088768641fb1c64756ee82b9d0b088b2950f6bae961dccc19" "e3d4bc76062df5a4b72d0ec8abd04233ea3d33a8418430a719bd533e0e7c36ee" "14afbe72aeadc67fb3061ddaaaad924c58b45dc6c604fa31675222d851b70161" "9a4b5a8dfe01489c7046f75764c988a8a93aeb36e48c39dc55f21fb3ecbf2c59" "5c5de678730ceb4e05794431dd65f30ffe9f1ed6c016fa766cdf909ba03e4df4" "087af241cc83130395244db7fca84b545fef75da92d63462436f3ffef916fa33" "6ad45ae62cb64d84c6febbe85e3da7b3e68e65383e1a333578cabe5398d3109a" "5f4dfda04fbf7fd55228266c8aab73953d3087cea7fd06dd7f8ff1e4a497c739" "e006d90eaf64a2d78154ecb277ccc82327e1b975d7d0e2f933acca3131cd0177" "9939e735844cb24144d29ddf03fadf86a2d455758afeeee30372258e8a6401bb" "c29d6bc4bb6ee61149ce0fce6bc2bea539dcab11f0fb7bb0f1e2d8f5fb817a8e" "77a70a104f0eefd1bade76d53ba6cadfbb6c87eeb9f358acd85d6c5ce9aa02d6" "4b0e826f58b39e2ce2829fab8ca999bcdc076dec35187bf4e9a4b938cb5771dc" "8d7b028e7b7843ae00498f68fad28f3c6258eda0650fe7e17bfb017d51d0e2a2" "cbdf8c2e1b2b5c15b34ddb5063f1b21514c7169ff20e081d39cf57ffee89bc1e" "8310462f5008fe10661e27ceab164e05c509343641cc262134fc623422b5999d" "ded82bed6a96cb8fdc7a547ef148679e78287664a5236e9c694e917383b052d7" "dbcaaf13f0d62b577dedefe6e1a9ae3e57ac86f316fea6ae83341db916fcb9c7" "57db540d6a8cc20d2e2f20bd63dc3af4eb9e4bbfa7252a0ee877c99b577996c4" "026eff18f1c0fda8f9bf5738343019a8ebe8db55c1bd9bcbd4536d8ed112d5df" "5e769f0dc4c262f216c2a30ca8bf55ff2ebc164f779bd2f32ce989290dc13485" "dd8223836a1a1576a8e9e7e1d19a894dce45e0f1a5498b5aecf0ccef3bec8b90" "fbe90e506c7bbc25eb1cbfce3bcd90a2bec9fe09ecfdf7302fc8705d43a39fcd" "9493b7c662667d7a2f2bf80390d88f549e1e253f3f647be575d9f29b215ef2c6" "9ac11c78f208abf58e5b313a33147cbf209ad9dc9cb169bf82464b043b45ad7a" "a9a67b318b7417adbedaab02f05fa679973e9718d9d26075c6235b1f0db703c8" "7a7b1d475b42c1a0b61f3b1d1225dd249ffa1abb1b7f726aec59ac7ca3bf4dae" "0d01e1e300fcafa34ba35d5cf0a21b3b23bc4053d388e352ae6a901994597ab1" "a6e620c9decbea9cac46ea47541b31b3e20804a4646ca6da4cce105ee03e8d0e" "8146edab0de2007a99a2361041015331af706e7907de9d6a330a3493a541e5a6" "333958c446e920f5c350c4b4016908c130c3b46d590af91e1e7e2a0611f1e8c5" "246cd0eb818bfd347b20fb6365c228fddf24ab7164752afe5e6878cb29b0204e" "7356632cebc6a11a87bc5fcffaa49bae528026a78637acd03cae57c091afd9b9" "04dd0236a367865e591927a3810f178e8d33c372ad5bfef48b5ce90d4b476481" "ab04c00a7e48ad784b52f34aa6bfa1e80d0c3fcacc50e1189af3651013eb0d58" "a0feb1322de9e26a4d209d1cfa236deaf64662bb604fa513cca6a057ddf0ef64" "3d4df186126c347e002c8366d32016948068d2e9198c496093a96775cc3b3eaa" "ea2ca601357f716bea02a6129dc6ce8b065c68eedf8d4e2cdf38189c5cf31a61" "738c4838957c1884dfacbb6f4f783c54e87c4a6b31c336d6279fc1c2b2ee56c5" "0615f6940c6c5e5638c9157644263889db755d43576c25f7b311806f4cfe2c3a" "cdc2a7ba4ecf0910f13ba207cce7080b58d9ed2234032113b8846a4e44597e41" "a0d9281cf41e8a226f0539a7f54e4812fdeaaec36c751b84671df97a54013465" "69ad4071c7b2d91543fddd9030816404ff22e46f7207549319ce484e23082dee" "46f6f73fb47a2a19b6ee1a49781f835fd73a185674268d4e048acf6feac9c55d" "654b365467a92ff70c70f4926974e07dcdb34805d2787c51710b467e695342e6" "c51e302edfe6d2effca9f7c9a8a8cfc432727efcf86246002a3b45e290306c1f" "2ea9afebc23cca3cd0cd39943b8297ce059e31cb62302568b8fa5c25a22db5bc" "0ec7094cc0a201c1d6f7c37f2414595d6684403b89b6fd74dcc714b5d41cd338" "45482e7ddf47ab1f30fe05f75e5f2d2118635f5797687e88571842ff6f18b4d5" "e624f013e266f41148aa2e445a4b8681b0afb346a9126993e345309c9a829535" "7de92d9e450585f9f435f2d9b265f34218cb235541c3d0d42c154bbbfe44d4dd" "0f302165235625ca5a827ac2f963c102a635f27879637d9021c04d845a32c568" "1866ad9491a157007f89c14ceb54dfd04d8c1b5015132c79975331276eefef50" "c02b12444e027c332e58a7fb46ffd870df9e00123fd61e675288439b22c3c2a1" "c3fa63eab93d1f0b4bf9f60a98a2848ba29c34cc6f2ef5cf4076d9c190a47a6c" "41a5a10e5221572b54c478eba62b51c510277100c9553d198fb1f4d56c45c6da" "011d4421eedbf1a871d1a1b3a4d61f4d0a2be516d4c94e111dfbdc121da0b043" "922f930fc5aeec220517dbf74af9cd2601d08f8250e4a15c385d509e22629cac" "9568c83e0af1d7a992c37411ace790036735a26c484531bf77690adf2c5d3e8b" "bd28a7a54d9bfbda4456afb650a5990282b391f1e0494fb04b095981255066ae" "20ad8133a73088c0ce4f26d106c3e70cae4e10c7e613c9b9e17a962eb194a24f" "047ec205dcb5edbb94b35800110150a6e41e6cc92c0ccfb2ed25ac3df94331a6" "b5cff93c3c6ed12d09ce827231b0f5d4925cfda018c9dcf93a2517ce3739e7f1" "64affc3597b4271ba6b0b428777d616cfb20d8f7f147dbd00f1de220b2b59bbf" "4ca84f9861113f889d505b66c4a9c30b81400f450b54f4c857d3eb44c5bdc601" "1ba950bff674dd0e08edeb8b8f83ee338bbef0bc769349ed38de10854927e487" "aad7fd3672aad03901bf91e338cd530b87efc2162697a6bef79d7f8281fd97e3" "b80d4f6dee7691fc5a437d760164c3eba202944b3f977d5b47bbb6b76cba0806" "69ecb7a75a0a8440df4b9ffe28b46dadf849b499c7e10680c26b99a84df773ca" "d1b30c69aaf27807108e358b08c6aec4759d7240bc530fca90fd5833813fbc84" "32b75b124f7f84517d3803a13a807189bae717b9d28e68e5a327a8ba55b7a6ee" "57d7e8b7b7e0a22dc07357f0c30d18b33ffcbb7bcd9013ab2c9f70748cfa4838" "c2bce71b37ffd6e95fbd3b98d6eaadd113ec308f85149cfc8f50dee716764fed" "9dc64d345811d74b5cd0dac92e5717e1016573417b23811b2c37bb985da41da2" "6cf0e8d082a890e94e4423fc9e222beefdbacee6210602524b7c84d207a5dfb5" "f831c1716ebc909abe3c851569a402782b01074e665a4c140e3e52214f7504a0" "d1527d35673f958d370d23096a266888771d68f5942be5377796d79ab2c9792f" "39f0ac86b012062fed46469cc5ea1b00aa534db587ad21d55a9717a1bac99a27" "4c8372c68b3eab14516b6ab8233de2f9e0ecac01aaa859e547f902d27310c0c3" "67b11ee5d10f1b5f7638035d1a38f77bca5797b5f5b21d16a20b5f0452cbeb46" "3fe4861111710e42230627f38ebb8f966391eadefb8b809f4bfb8340a4e85529" "58ef0d9d877d9b1c19183f4215de272fa2938744f077e3a69ea12b8b6e852155" "d9c957b0e8d2d7f1bbb781fc729e06598017ade2d0c18611e5abbdde0f65d981" "a4ef58c2fb31b065ad09fa3029adba5eb518e42ef104cf9acf7c409abf40ca55" "1319bfcf42cefb12fad274e5763d0eae0d0401cddc9738bdf220fe89447e9292" "8c867d38498653a86c1071714502d01eabb8d8c9ec976a7ddc4d04c23a991a97" "10551f0a24d0ac97a109f02178e9e34b448ee12a52357911cf3362a6b249cae6" "dd7213b37f448685f41e28b83a497f78fdefeeef0d47531fc24e99f576a7a191" "3d9df5511048d0815b1ccc2204cc739117c1a458be92fb26c03451149a1b1c11" "6213a6047cc19f580c37ef3f6d47fd5a55ebdf9b5590475d8f7a6aecd79a1cc0" "34c1b320a9d35318ca01660d533eee299d538f5a0c505c076511493b0a4f093d" "ff8be9ed2696bf7bc999423d909a603cb23a9525bb43135c0d256b0b9377c958" "23b564cfb74d784c73167d7de1b9a067bcca00719f81e46d09ee71a12ef7ee82" "0231f20341414f4091fc8ea36f28fa1447a4bc62923565af83cfb89a6a1e7d4a" "f0c94bf6a29c232300e46af50f46ce337e721eacca6d618e8654a263db5ecdbe" "4641f2added941818ca5a618aa38206d6dd6c2fa553137e2d0e1c82073b8674a" "e76668f7efa2c9297c3998aba61876e757c99c1e5325c135433cf4f53a95df15" "f99318b4b4d8267a3ee447539ba18380ad788c22d0173fc0986a9b71fd866100" "e3a1b1fb50e3908e80514de38acbac74be2eb2777fc896e44b54ce44308e5330" "fb83a50c80de36f23aea5919e50e1bccd565ca5bb646af95729dc8c5f926cbf3" "24168c7e083ca0bbc87c68d3139ef39f072488703dcdd82343b8cab71c0f62a7" "0f2f1feff73a80556c8c228396d76c1a0342eb4eefd00f881b91e26a14c5b62a" "bbd478fcc11adf3e314bfb4d326197e9492095a58739c2cce6cad97884b186e5" "3147f60a6e56480728bd702d3874a8927efee05bc5fea43b9b6fc9a6fa45b331" "13a8eaddb003fd0d561096e11e1a91b029d3c9d64554f8e897b2513dbf14b277" "7f1d414afda803f3244c6fb4c2c64bea44dac040ed3731ec9d75275b9e831fe5" "51ec7bfa54adf5fff5d466248ea6431097f5a18224788d0bd7eb1257a4f7b773" "a60d5f9dbe91c84b5c83685d9e7a535bd0b2283a5c4fd226837711004c9dcfe2" "28eb6d962d45df4b2cf8d861a4b5610e5dece44972e61d0604c44c4aad1e8a9d" "ea287582eaea4e0c0a991ad0aa42f679f21b8d92766762bcc4d730d4aa6c8fd7" "5eed5311ae09ed84cb2e4bf2f033eb4df27e7846a68e4ea3ab8d28f6b017e44a" "d0e0067bb5f8ead66eaed3d9adfeed64a4de34f4e377633f0ff4112151558a6a" "e0628ee6c594bc7a29bedc5c57f0f56f28c5b5deaa1bc60fc8bd4bb4106ebfda" "48c02faac1209d3e960e15fa252cc0869aab5f96e6533bcf4619d2e88b74d7ac" "57e3f215bef8784157991c4957965aa31bac935aca011b29d7d8e113a652b693" default))
 '(display-time-mode t)
 '(ede-project-directories
   '("g:/My Drive/src/tmp/myproject/include" "g:/My Drive/src/tmp/myproject/src" "g:/My Drive/src/tmp/myproject" "g:/My Drive/src/tmp"))
 '(ein:jupyter-default-notebook-directory
   "c:/Users/micha/Dropbox/src/python/bookcamp_code/Case_Study1")
 '(elpy-mode-hook '(subword-mode hl-line-mode))
 '(elpy-modules
   '(elpy-module-company elpy-module-eldoc elpy-module-pyvenv elpy-module-highlight-indentation elpy-module-yasnippet elpy-module-sane-defaults))
 '(elpy-rpc-virtualenv-path 'default)
 '(elpy-syntax-check-command "pyflakes")
 '(elpy-test-runner 'elpy-test-pytest-runner)
 '(emms-player-list '(emms-player-mplayer-playlist emms-player-mplayer))
 '(emms-player-mplayer-command-name "c:/tools/mplayer/mplayer.exe")
 '(ess-directory-containing-R "\"C:/Program Files/R/R-4.1.2/bin/x64\"")
 '(exec-path
   '("c:/Program Files (x86)/Calibre2/" "c:/Users/micha/null/Coursier/data/bin" "C:/Program Files/Racket" "C:/ProgramData/chocolatey/lib/mpv.install/tools" "c:/tools/mplayer/" "C:/Program Files (x86)/SMLNJ/bin" "c:/tools/apache-ant-1.10.12/bin" "c:/tools/ffmpeg/bin" "C:/GNU-Prolog/bin" "C:/Program Files (x86)/Lua/5.1" "c:/tools/ghc-9.0.1/bin" "c:/tools/fsharp" "c:/Users/micha/bin" "c:/Users/micha/AppData/Roaming/Python/Python310/Scripts" "c:/Python310" "c:/Python310/Scripts" "C:/Program Files/Git/cmd" "c:/tools/ruby30/bin" "c:/users/micha/OneDrive/src/powershell" "C:/Program Files/Chez Scheme 9.5.4/bin/ta6nt" "C:/tools/Julia-1.6.1/bin" "C:/tools/apache-maven-3.8.1/bin" "c:/rakudo/bin" "C:/tools/ghostwriter" "C:/Program Files/Haskell Platform/8.6.5/lib/extralibs/bin" "C:/Program Files/Haskell Platform/8.6.5/bin" "C:/Emacs/x86_64/bin/" "C:/Users/micha/AppData/Local/.meteor" "C:/Program Files/Common Files/Oracle/Java/javapath/" "C:/Users/micha/AppData/Local/atom/bin" "C:/tools/clisp-2.49" "C:/tools/nim-1.4.0/bin" "C:/WINDOWS/system32" "C:/WINDOWS" "C:/WINDOWS/System32/Wbem" "C:/WINDOWS/System32/WindowsPowerShell/v1.0/" "C:/WINDOWS/System32/OpenSSH/" "C:/Program Files (x86)/Brackets/command" "C:/Program Files/Microsoft VS Code/bin" "C:/Program Files/Microsoft SQL Server/130/Tools/Binn/" "C:/Program Files/Microsoft SQL Server/Client SDK/ODBC/170/Tools/Binn/" "C:/Program Files/gnuplot/bin" "C:/Program Files (x86)/scala/bin" "C:/Program Files/dotnet/" "C:/ProgramData/chocolatey/bin" "C:/Users/micha/AppData/Roaming/nvm" "C:/Program Files/nodejs" "C:/Program Files/jEdit" "C:/Program Files/Haskell Platform/8.6.5/mingw/bin" "C:/Program Files/Docker/Docker/resources/bin" "C:/ProgramData/DockerDesktop/version-bin" "C:/Program Files (x86)/Gpg4win/../GnuPG/bin" "C:/Program Files/MariaDB/MariaDB Connector C 64-bit/lib/" "C:/Program Files/MariaDB/MariaDB Connector C 64-bit/lib/plugin/" "C:/Program Files (x86)/Calibre2/" "C:/Program Files/Go/bin" "C:/ProgramData/ComposerSetup/bin" "C:/Program Files (x86)/sbt/bin" "C:/Program Files/PowerShell/7/" "C:/Program Files/OpenJDK/jdk-16.0.2/bin" "C:/Program Files/GNU Octave/Octave-6.3.0" "C:/Program Files/GNU Octave/Octave-6.3.0/mingw64/bin" "c:/tools/unzip" "c:/Program Files/7-Zip" "C:/Program Files/WinZip" "C:/tools/clojure" "C:/tools/maven/bin" "C:/tools/scala3" "C:/Users/micha/scala/Coursier/data/bin" "C:/msys64/mingw64/bin" "C:/Program Files/WinRAR" "C:/tools/php-8.0.9" "C:/tools/fsharp" "C:/Program Files/R/R-4.1.2/bin/x64" "C:/tools/dart-sass" "C:/tools/SQLiteStudio" "C:/Program Files/CMake/bin" "C:/tools/fasmw17327" "C:/Users/micha/.cargo/bin" "C:/Users/micha/.rustup/toolchains/stable-x86_64-pc-windows-msvc/bin" "C:/Users/micha/AppData/Local/Programs/Python/Launcher/" "C:/Users/micha/AppData/Roaming/local/bin" "C:/Program Files/MongoDB/Server/4.4/bin" "C:/tools/mongodb-database-tools/bin" "C:/Users/micha/AppData/Roaming/npm/bin" "C:/PROGRA~1/Gambit/v4.9.3/bin" "C:/Users/micha/AppData/Local/Microsoft/WindowsApps" "C:/Users/micha/AppData/Local/atom/bin" "C:/tools/nim-1.4.0/bin" "C:/Users/micha/AppData/Roaming/npm" "C:/Users/micha/AppData/Local/.meteor" "C:/ProgramData/chocolatey/lib/ghc/tools/ghc-8.10.4/bin" "C:/tools/msys64" "c:/Users/micha/AppData/Local/Microsoft/WindowsApps" "C:/Users/micha/AppData/Roaming/nvm" "C:/Program Files/nodejs" "C:/tools/dart-sdk/bin" "C:/Program Files/Docker Toolbox" "C:/tools/Julia-1.6.1/bin" "C:/rakudo/bin" "C:/rakudo/share/perl6/site/bin" "c:/Emacs/emacs-28/libexec/emacs/28.0.50/x86_64-w64-mingw32" "c:/Users/micha/bin" "c:/texlive/2021/bin/win32" "c:/Python310" "c:/Python310/Scripts" "C:/msys64/usr/bin" "c:/tools/tern/bin" "c:/Users/micha/AppData/Roaming/.emacs.d/elpa/flycheck-pycheckers-20210414.2023/bin" "C:/Users/micha/AppData/Local/Pandoc"))
 '(flycheck-c/c++-clang-executable "C:/Program Files/LLVM/bin/clang.exe")
 '(flycheck-disabled-checkers '(emacs-lisp-checkdoc))
 '(flycheck-emacs-lisp-load-path 'inherit)
 '(flycheck-pycheckers-checkers '(pylint pyflakes mypy3))
 '(flycheck-pycheckers-venv-root "c:/Users/micha/.venv")
 '(flycheck-python-flake8-executable "C:/Python310/Scripts/flake8.exe")
 '(flycheck-python-mypy-executable "c:/Python310/Scripts/mypy.exe")
 '(flycheck-python-pylint-executable "c:/Python310/Scripts/pylint.exe")
 '(flyspell-default-dictionary nil)
 '(fortran-tab-mode-default t)
 '(go-command "C:/Program Files/Go/bin/go.exe")
 '(helm-buffers-column-separator " | ")
 '(helm-use-frame-when-dedicated-window t)
 '(help-at-pt-display-when-idle '(flymake-overlay) nil (help-at-pt))
 '(help-at-pt-timer-delay 0.3)
 '(highlight-changes-colors '("#EF5350" "#7E57C2"))
 '(highlight-symbol-colors
   '("#55204c0039fc" "#3f0a4e4240dc" "#5a2849c746fd" "#3fd2334a42f4" "#426a4d5455d9" "#537247613a13" "#46c549b0535c"))
 '(highlight-symbol-foreground-color "#999891")
 '(highlight-tail-colors
   '(("#010F1D" . 0)
     ("#B44322" . 20)
     ("#34A18C" . 30)
     ("#3172FC" . 50)
     ("#B49C34" . 60)
     ("#B44322" . 70)
     ("#8C46BC" . 85)
     ("#010F1D" . 100)))
 '(hl-bg-colors
   '("#4c4536" "#4b4136" "#504341" "#4d3936" "#3b313d" "#41434a" "#3b473c" "#3d464c"))
 '(hl-fg-colors
   '("#2a2a29" "#2a2a29" "#2a2a29" "#2a2a29" "#2a2a29" "#2a2a29" "#2a2a29" "#2a2a29"))
 '(hl-paren-colors '("#7ec98f" "#e5c06d" "#a4b5e6" "#834c98" "#8ac6f2"))
 '(inf-mongo-command "c:/Program Files/mongosh/mongosh.exe 127.0.0.1:27017")
 '(inferior-julia-program "c:/tools/Julia-1.6.1/bin/julia.exe")
 '(inferior-lisp-program "clisp" t)
 '(ispell-dictionary nil)
 '(ispell-highlight-face 'flyspell-incorrect)
 '(ispell-local-dictionary nil)
 '(jdee-server-dir "c:/tools/jdee-server")
 '(jedi:goto-definition-config
   '((nil nil nil)
     (t nil nil)
     (nil definition nil)
     (t definition nil)
     (nil nil t)
     (t nil t)
     (nil definition t)
     (t definition t)))
 '(jedi:install-imenu t)
 '(jedi:key-complete "C-<tab>")
 '(jedi:use-shortcuts t)
 '(julia-program "c:/tools/Julia-1.6.1/bin/julia.exe")
 '(kill-do-not-save-duplicates t)
 '(linum-format " %3i ")
 '(ls-lisp-emulation 'MS-Windows)
 '(lsp-client-packages
   '(lsp-julia ccls lsp-actionscript lsp-ada lsp-angular lsp-bash lsp-beancount lsp-clangd lsp-clojure lsp-cmake lsp-crystal lsp-csharp lsp-css lsp-d lsp-dart lsp-dhall lsp-dockerfile lsp-elm lsp-elixir lsp-erlang lsp-eslint lsp-fortran lsp-fsharp lsp-gdscript lsp-go lsp-graphql lsp-hack lsp-grammarly lsp-groovy lsp-haskell lsp-haxe lsp-java lsp-javascript lsp-jedi lsp-json lsp-kotlin lsp-ltex lsp-lua lsp-markdown lsp-nim lsp-nix lsp-metals lsp-ocaml lsp-perl lsp-php lsp-pwsh lsp-pyls lsp-pylsp lsp-python-ms lsp-purescript lsp-r lsp-rf lsp-rust lsp-solargraph lsp-sorbet lsp-tex lsp-terraform lsp-v lsp-vala lsp-verilog lsp-vetur lsp-vhdl lsp-vimscript lsp-xml lsp-yaml lsp-sqls lsp-svelte lsp-steep lsp-zig))
 '(lsp-pylsp-plugins-flake8-exclude [])
 '(lsp-pylsp-plugins-flake8-filename [])
 '(lsp-pylsp-plugins-flake8-ignore [])
 '(lsp-pylsp-plugins-flake8-select [])
 '(lsp-pylsp-plugins-mccabe-enabled nil)
 '(lsp-pylsp-plugins-pycodestyle-exclude [])
 '(lsp-pylsp-plugins-pydocstyle-add-ignore [])
 '(lsp-pylsp-plugins-pydocstyle-ignore [])
 '(lsp-pylsp-plugins-pyflakes-enabled nil)
 '(lsp-pylsp-plugins-pylint-enabled nil)
 '(lsp-pylsp-plugins-rope-completion-enabled t)
 '(lsp-pylsp-plugins-yapf-enabled t)
 '(lsp-pylsp-rope-rope-folder [])
 '(lsp-rust-analyzer-diagnostics-disabled ["unlinked-file"])
 '(lsp-ui-doc-border "#999891")
 '(magit-diff-use-overlays nil)
 '(magit-git-executable "C:/Program Files/Git/cmd/git.exe")
 '(nov-unzip-program "c:/Program Files/7-Zip/7z.exe")
 '(nrepl-message-colors
   '("#ffb4ac" "#ddaa6f" "#e5c06d" "#3d464c" "#e3eaea" "#41434a" "#7ec98f" "#e5786d" "#834c98"))
 '(org-agenda-files
   '("g:/My Drive/org/calendar.org" "g:/My Drive/org/tasks.org" "g:/My Drive/org/daily.org" "g:/My Drive/org/chores.org" "g:/My Drive/org/chess.org" "g:/My Drive/org/study.org"))
 '(org-babel-haskell-compiler "C:/tools/ghc-9.0.1/bin/ghc")
 '(org-babel-js-cmd "node")
 '(org-babel-julia-command "C:/tools/Julia-1.6.1/bin/julia.exe")
 '(org-babel-load-languages
   '((awk . t)
     (emacs-lisp . t)
     (ruby . t)
     (octave . t)
     (awk . t)))
 '(org-babel-lua-command "C:/Program Files (x86)/Lua/5.1/lua")
 '(org-babel-python-command "C:/Python310/python.exe")
 '(org-gcal-down-days 30)
 '(org-gcal-local-timezone "America/New_York")
 '(org-gcal-up-days 15)
 '(org-journal-dir "g:/My Drive/org/journal")
 '(org-journal-prefix-key "C-c j")
 '(org-log-into-drawer t)
 '(org-refile-targets '((org-agenda-files :maxlevel . 2)))
 '(org-superstar-item-bullet-alist '((42 . 8226) (43 . 10148) (45 . 167)))
 '(package-gnupghome-dir "c:/Users/micha/AppData/Roaming/.emacs.d/elpa/gnupg")
 '(package-native-compile t)
 '(package-quickstart t)
 '(package-selected-packages
   '(ob-mermaid alt-codes sbt-mode shell-here psc-ide julia-formatter company-reftex latex-preview-pane helm-spotify-plus rubocop ruby-electric geiser-racket racer utop ob-sml geiser-chez geiser pippel helm-tramp ant mvn sml-basis sml-mode merlin-eldoc flycheck-ocaml learn-ocaml arc-dark-theme ob-ess-julia ob-rust ob-go ob-fsharp ediprolog ob-prolog lua-mode html2org org-babel-eval-in-repl ob-typescript eshell-toggle js-format jsformat flycheck-flow company-flow js2hl jsfmt nodejs-repl flow-js2-mode ac-js2 clang-format+ phpstan use-package clang-format clippy cl-libify helm-company company-org-block flycheck-rust org-fancy-priorities java-imports helm-ag dakrone-theme abyss-theme org zettelkasten slime-company py-smart-operator org-superstar company-go helm-go-package go-snippets go-projectile go-imenu go-eldoc lsp-javacomp "0xc" helm-org yasnippet mmm-mode ob-mongo inf-mongo use-package-ensure-system-packages helm-system-packages composer flycheck-phpstan php-refactor-mode php-eldoc flymake-php org-books js2-refactor flylisp adjust-parens sexy-monochrome-theme rimero-theme reykjavik-theme overcast-theme obsidian-theme night-owl-theme monokai-pro-theme lush-theme klere-theme jbeans-theme iceberg-theme hybrid-reverse-theme green-is-the-new-black-theme gotham-theme espresso-theme dream-theme commentary-theme chocolate-theme gulp-task-runner helm-projectile sphinx-doc savekill python-switch-quotes python-info pygn-mode gcmh flyspell-correct-helm flycheck-julia helm-lsp pdb-capf xref-js2 web-server web-mode-edit-element vue-mode vc-hgcmd use-package-chords ts-comint toggle-quotes tern scala-mode rust-playground rust-auto-use restart-emacs quack python pytest pylint prettier-js prettier popup-imenu nimbus-theme lsp-dart live-py-mode js-comint jedi java-snippets init-loader imenu-anywhere htmltagwrap html-check-frag haskell-tab-indent haskell-snippets gnu-elpa-keyring-update gitignore-snippets gitignore-mode gitconfig-mode flycheck-inline ess-smart-equals ess-R-data-view emmet-mode elisp-slime-nav dotnet discover dired-git company-quickhelp company-jedi company-ghci common-lisp-snippets clojure-essential-ref cargo-mode browse-at-remote auto-rename-tag ag afternoon-theme ac-slime xterm-color xcscope wisi websocket web-completion-data uniquify-files treemacs-projectile treemacs-icons-dired tablist string-inflection smart-compile sesman request-deferred request reformatter queue pyvenv python-docstring python-black pretty-speedbar persist org-roam-ui org-roam-timestamps org-roam-bibtex org-pdftools org-noter org-gcal org-dotemacs marshal lsp-julia lsp-haskell logito log4e kv key-assist json-snatcher json-mode inf-ruby htmlize hover highlight-indentation gnuplot gntp github-clone gh general folding focus flymake-go flymake-eslint flymake-diagnostic-at-point flycheck-pycheckers face-explorer eshell-git-prompt emacsql-sqlite elpy elpl elisp-refs dockerfile-mode docker-tramp diredc diminish danneskjold-theme csharp-mode company-web company-php citeproc cargo bui biblio-core biblio auto-package-update ammonite-term-repl all-the-icons-dired alert aio ada-ref-man ada-mode))
 '(podcaster-feeds-urls
   '("https://ipn.li/kernelpanic/feed" "http://sachachua.com/blog/tag/emacs-chat/podcast" "https://play.acast.com/s/not-just-the-tudors"))
 '(podcaster-mp3-player "C:/tools/ffmpeg/bin/ffplay.exe")
 '(pos-tip-background-color "#FFF9DC")
 '(pos-tip-foreground-color "#011627")
 '(prettier-js-args nil)
 '(prettier-js-command "c:/Users/micha/AppData/Roaming/npm/prettier.cmd")
 '(prettier-js-width-mode 'fill)
 '(pretty-speedbar-icon-size 40)
 '(projectile-dirconfig-comment-prefix ";")
 '(prolog-compile-string
   '((eclipse "[%f].")
     (mercury "mmake ")
     (sicstus
      (eval
       (if
	   (prolog-atleast-version
	    '(3 . 7))
	   "prolog:zap_file(%m,%b,compile,%l)." "prolog:zap_file(%m,%b,compile).")))
     (swi "[%f].")
     (t "compile(%f).")
     (gnu "c:/GNU-Prolog/bin/gplc.exe %f")))
 '(prolog-program-name
   '((gnu "c:/GNU-Prolog/bin/gplc.exe")
     ((getenv "EPROLOG")
      (eval
       (getenv "EPROLOG")))
     (eclipse "eclipse")
     (mercury nil)
     (sicstus "sicstus")
     (swi "pl")
     (t "c:/GNU-Prolog/bin/gprolog.exe")))
 '(prolog-system 'gnu)
 '(prolog-system-version
   '((sicstus
      (3 . 6))
     (swi
      (0 . 0))
     (mercury
      (0 . 0))
     (eclipse
      (3 . 7))
     (gnu
      (1 . 5))))
 '(py-ipython-command "c:/Python310/Scripts/ipython.exe")
 '(python-shell-interpreter "c:/Python310/python.exe")
 '(safe-local-variable-values '((pyvenv-workon . "temp-conv")))
 '(scheme-mit-dialect nil)
 '(scheme-program-name "C:/Program Files/Chez Scheme 9.5.4/bin/ta6nt/scheme.exe")
 '(show-paren-mode t)
 '(smartrep-mode-line-active-bg (solarized-color-blend "#8ac6f2" "#2f2f2e" 0.2))
 '(smtpmail-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-service 587)
 '(tags-table-list
   '("c:/Users/micha/AppData/Roaming/.emacs.d/.emacs-config/TAGS"))
 '(term-default-bg-color "#2a2a29")
 '(term-default-fg-color "#8d8b86")
 '(treemacs-tag-follow-delay 3)
 '(treemacs-tag-follow-mode t)
 '(vc-annotate-background-mode nil)
 '(warning-suppress-types '((comp) (comp)))
 '(weechat-color-list
   '(unspecified "#011627" "#010F1D" "#DC2E29" "#EF5350" "#D76443" "#F78C6C" "#D8C15E" "#FFEB95" "#5B8FFF" "#82AAFF" "#AB69D7" "#C792EA" "#AFEFE2" "#7FDBCA" "#D6DEEB" "#FFFFFF"))
 '(xterm-color-names
   ["#2f2f2e" "#ffb4ac" "#8ac6f2" "#e5c06d" "#a4b5e6" "#e5786d" "#7ec98f" "#e8e5db"])
 '(xterm-color-names-bright
   ["#2a2a29" "#ddaa6f" "#6a6a65" "#74736f" "#8d8b86" "#834c98" "#999891" "#f6f3e8"])
 '(yas-global-mode t)
 '(yas-snippet-dirs
   '("c:/Users/micha/AppData/Roaming/.emacs.d/snippets" "c:/Users/micha/AppData/Roaming/.emacs.d/elpa/haskell-snippets-20210228.344/snippets" "c:/Users/micha/AppData/Roaming/.emacs.d/elpa/common-lisp-snippets-20180226.1523/snippets" "c:/Users/micha/AppData/Roaming/.emacs.d/elpa/java-snippets-20160627.252/snippets" "c:/Users/micha/AppData/Roaming/.emacs.d/elpa/elpy-20220322.41/snippets/"))
 '(zettelkasten-directory "c:/Users/micha/AppData/Roaming/zettel")
 '(zettelkasten-prefix "\3k"))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(fixed-pitch ((t (:height 100 :family "Cascadia Mono PL"))))
 '(font-lock-comment-face ((t (:foreground "slate gray"))))
 '(font-lock-variable-name-face ((t (:foreground "medium sea green"))))
 '(helm-buffer-directory ((t (:extend t :background "dark turquoise" :foreground "navy"))))
 '(helm-selection ((t (:extend t :background "indian red" :distant-foreground "black"))))
 '(org-document-title ((t (:foreground "gold" :weight bold :height 1.5 :family "Kodchasan"))))
 '(org-level-1 ((t (:inherit outline-1 :extend nil :height 1.2))))
 '(org-level-2 ((t (:inherit outline-2 :extend nil :weight normal :height 1.0))))
 '(org-meta-line ((t (:inherit (fixed-pitch font-lock-comment-face default) :height 110 :width normal :family "Roboto Mono"))))
 '(org-tag ((t (:weight normal))))
 '(org-warning ((t (:inherit font-lock-warning-face :weight normal))))
 '(outline-2 ((t (:foreground "pale goldenrod" :weight bold :height 1.1))))
 '(whitespace-big-indent ((t (:background "peru" :foreground "burlywood"))))
 '(whitespace-empty ((t (:extend t :foreground "light salmon" :inverse-video t :underline nil)))))

(provide 'custom)
;;; custom.el ends here
