;; batch run emacs to install packages.

(require 'package)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

(package-initialize)

(package-install  '0xc)
(message "installed package 0xc")
