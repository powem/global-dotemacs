;;; .prog-modes.el --- Settings for programming modes -*- coding: utf-8 -*- lexical-binding: t -*-
;;;
;;; Commentary:
;;;
;;; Sat Nov 20, 2021  7:59 PM
;;;
;;; Time-stamp: <2021-08-26 16:15:37 micha>
;;; `(with-suppressed-warnings)' is applied to places
;;; generating 'free variable' and 'not known to be available'
;;; warnings.
;;;
;;; **** Byte compiling strips comments from the compiled file, ****
;;; **** so the best way to reduce file size is to comment out  ****
;;; **** items that are not to be used.****
;;;
;;; Author: mpowe
;;; Separating programming modes and setups to a file that won't
;;; break the .emacs file on startup
;;;
;;; This file has been edited in `emacs-config-prog-modes.org` as
;;; literate programming, and changes should be made there.
;;;
;;; Code:
;;;
;;;
;;;
(require '.emacs-config-vars)

;; change some font locking for emacs lisp, I don't like some of
;; the defaults.
;; Fri Jul 30, 2021  9:23 PM
;;
(font-lock-add-keywords 'emacs-lisp-mode
                        '(("\\<\\(add-to-list\\|file-newer-than-file-p\\|byte-compile-file\\|file-exists-p\\|add-hook\\|concat\\)\\>"
                           . font-lock-function-name-face)
                          ("\\<\\(put\\|message\\|and\\|or\\|not\\)\\>" . font-lock-keyword-face)))

;; (font-lock-add-keywords 'c-mode
;; 			'(("\\<\\(FIXME\\):" 1 font-lock-warning-face prepend)
;; 			  ("\\<\\(and\\|or\\|not\\)\\>" . font-lock-keyword-face)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;	      Enable/disable specific programming modes
;;;		      Wed Jul 14, 2021 10:37 AM
;;;	     Uncomment to enable. Comment out to disable.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Modes that are commented out (set to `nil') are not tangled! They
;;; won't show up in this file unless the `:tangle'`no' cookie is
;;; removed.

;;; (setq my-use-package-csharp-p t)     ;; Mode for editing C# code
;;; (setq my-use-package-ess-p t)        ;; ESS for R
(setq my-use-package-javascript-p t) ;; JavaScript editing packages
(setq my-use-package-python-p t)     ;; Elpy & friends for python
;;; (setq my-use-package-docker-p t)     ;; enable docker & dockerfile editing
;;; (setq my-use-package-clojure-p t)    ;; cider for clojure
;;; (setq my-use-package-dart-p t)       ;; Dart & Flutter editing.
;;; (setq my-use-package-fsharp-p t)     ;; Use F# mode
;;; (setq my-use-package-go-mode-p t)    ;; Use Go mode
;;; (setq my-use-package-haskell-p t)    ;; Haskell editing mode
;;; (setq my-use-package-jdee-p t)       ;; JDEE for Java (Not installed - do not enable!)
;;; (setq my-use-package-jmt-mode-p t)       ;; Java Mode Tamed
;;; (setq my-use-package-julia-p t)      ;; Julia mode
;;; (setq my-use-package-meghanada-p t)  ;; Meghanada mode for Java
;;; (setq my-use-package-nim-p t)        ;; nim mode
;;; (setq my-use-package-php-p t)        ;; PHP editing mode
;;; (setq my-use-package-powershell-p t) ;; Powershell editing mode
;;; (setq my-use-package-quack-p t)      ;; Quack for Scheme
;;; (setq my-use-package-rust-p t)       ;; Rust editing packages
;;; (setq my-use-package-scala-p t)      ;; Scala mode programming
;;; (setq my-use-package-slime-p t)      ;; SLIME for Common Lisp
;;; (setq my-use-package-tide-p t)       ;; TIDE for TypeScript
(setq my-use-package-web-p t)        ;; web mode editing packages

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;			begin Global Settings
;;;		      Sat Jun 26, 2021 12:59 AM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This is being set in the vars file, not needed here.
;; (setq ctags-path "C:\\Emacs\x86_64\\bin\\ctags.exe")
;; multi-major-mode autoload
;; Sun Aug 29, 2021  4:25 PM
(require 'mmm-auto)

;; add functionality to get repos from github directly
;; Wed Jun 23, 2021 11:02 AM
;;
;; package deleted Sat Oct 30, 2021  8:17 PM
;; (use-package github-clone :defer t :bind ("C-c m h" . github-clone))
;; speedbar
(use-package speedbar :defer t :bind ("C-c ms" . speedbar)
  :config
  (setq speedbar-fetch-etags-command 'ctags-path
                speedbar-fetch-etags-arguments '("-f" "-")))

;; Was not working with `use-package'. Had to switch to `require'.
;; Also, had to set font specifically as below. Just borrowed these
;; colors from repo. Maybe get some better ones later.
;; Sun Feb 20, 2022  1:33 PM
;;
;; (use-package pretty-speedbar :defer t)
(require 'pretty-speedbar)

(setq pretty-speedbar-font "Font Awesome 6 Free Solid")
(setq pretty-speedbar-icon-fill "#FFFFFF") ;; Fill color for all non-folder icons.
(setq pretty-speedbar-icon-stroke "#DCDCDC") ;; Stroke color for all non-folder icons.
(setq pretty-speedbar-icon-folder-fill "#FAD7A0") ;; Fill color for all folder icons.
(setq pretty-speedbar-icon-folder-stroke "#CC00CC") ;; Stroke color for all folder icons.
(setq pretty-speedbar-about-fill "#922B21") ;; Fill color for all icons placed to the right of the file name, including checks and locks.
(setq pretty-speedbar-about-stroke "#DCDCDC") ;; Stroke color for all icons placed to the right of the file name, including checks and locks.
(setq pretty-speedbar-signs-fill "#1F618D") ;; Fill color for plus and minus signs used on non-folder icons.

;; elisp REPL
;; Sat Jul 31, 2021  6:03 AM
;;
;; package deleted Sat Oct 30, 2021  8:15 PM
;; (use-package elpl :defer t :bind (:map elpl-mode-map("C-c l" . elpl-clean)("C-c '" . elpl-edit)))
;;
;; mode-compile is dead since at least May 2020
;; replaced with smart-compile
;;
;; begin mode-compile
;; Sun Nov 27, 2011  9:35 PM
;;(autoload 'mode-compile "mode-compile"
;;  "Command to compile current buffer file based on the major mode" t)
;;(autoload 'mode-compile-kill "mode-compile"
;;  "Command to kill a compilation launched by `mode-compile'" t)
;; end mode-compile
;;
;; Displays a menu of keybindings available when a modifier key is pressed
(use-package which-key :defer t :config (which-key-mode))
;; Displays a popup menu of completion items for modifier keys in
;; dired, registers, and ESC-s search
(use-package discover :defer 30 :config (global-discover-mode 1))
;; Sun Jul  4, 2021  6:28 PM
;; Shows lines changed and how changed in left gutter for git-controlled items
(use-package git-gutter :defer t)
(global-git-gutter-mode t)
;; Sun Jul  4, 2021  6:29 PM
(use-package gitignore-mode :defer t)
(use-package yasnippet :defer t :config (yas-global-mode))
(use-package yasnippet-snippets :defer t :after yasnippet)
(use-package gitignore-snippets :defer t :after yasnippet)
;; (use-package flycheck :ensure t)

;; goto definitions
;; Tue Oct 12, 2021  4:24 PM
(use-package dumb-jump :defer t)
(add-hook 'xref-backend-functions #'dumb-jump-xref-activate)
(setq xref-show-definitions-function #'xref-show-definitions-completing-read)
;;
;; open a shell in working directory
;; Mon Feb  7, 2022  1:40 PM
(use-package shell-here :defer t)
;;
;; Edit mermaid.js files in emacs
;; Thu Mar 24, 2022  9:46 PM
(use-package mermaid-mode :defer t)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			 end Global Settings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package smart-compile :defer t)
(bind-key "C-c c" 'smart-compile)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;		 begin Language Server Protocol (LSP)
;;		      Tue Jun 29, 2021 11:50 AM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package lsp-mode :defer 40
  :init (setq lsp-keymap-prefix "C-c C-l")
  :config (setq lsp-completion-enable-additional-text-edit nil)
  :hook ((scala-mode . lsp)
         (lsp-mode . lsp-enable-which-key-integration)
         (lsp-mode . lsp-lens-mode)
         (java-mode . #'lsp-deferred)
         ;; (jmt-mode . #'lsp-deferred)
         (python-mode . lsp)
         )
  :commands lsp)
(use-package lsp-ui :after lsp-mode
  :init (setq lsp-ui-doc-delay 1.5
              lsp-ui-doc-position 'bottom
              lsp-ui-doc-max-width 100))
(use-package lsp-metals :defer t
    :custom
    (lsp-metals-server-args '("-J-Dmetals.allow-multiline-string-formatting=off"))
    :hook (scala-mode . lsp))

(use-package lsp-treemacs :after lsp-mode)
;;
;; debugger mode for use with LSP
;; general settings
;; Tue Jun 29, 2021 11:54 AM
(use-package dap-mode :after lsp-mode
  :config (dap-auto-configure-mode)
  :hook
  (lsp-mode . dap-mode)
  (lsp-mode . dap-ui-mode))
(dap-mode 1)
;; The modes below are optional
(dap-ui-mode 1) ;; enables mouse hover support
(dap-tooltip-mode 1) ;; use tooltips for mouse hover, if it is not
;; enabled `dap-mode' will use the minibuffer.
(tooltip-mode 1) ;; displays floating panel with debug buttons
;; requires emacs 26+
(dap-ui-controls-mode 1)
;; Posframe is a pop-up tool that must be manually installed for dap-mode
(use-package posframe)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;		  end Language Server Protocol (LSP)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;			    begin CC-Mode
;;;		      Tue Apr 27, 1999  1:51 PM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(cond ((and (equal use-cc-mode t) (file-exists-p "~/.emacs.d/.emacs-config/.cc-mode.elc"))
       (load-file "~/.emacs.d/.emacs-config/.cc-mode.elc"))
      ((and (equal use-cc-mode t) (file-exists-p "~/.emacs.d/.emacs-config/.cc-mode.el"))
       (load-file "~/.emacs.d/.emacs-config/.cc-mode.el")))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;			     end CC-Mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; a formatter for C/C++ and others, integrates the ClangFormat external tool
;; Sat Oct 16, 2021  2:56 PM
(use-package clang-format :defer t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			  begin Company mode
;;		      Tue Jun 29, 2021 11:21 PM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (moved from JavaScript editing section)
(with-suppressed-warnings
    ((use-package company
       :defines company-active-map
       :hook (scala-mode . company-mode)
       :custom
       (company-idle-delay 0.1)
       (global-company-mode t)
       (debug-on-error nil) ;; otherwise this throws lots of errors on completion errors
       :config
       (define-key company-active-map (kbd "TAB") 'company-complete-selection)
       (define-key company-active-map (kbd "<tab>") 'company-complete-selection)
       (define-key company-active-map [return] nil)
       (define-key company-active-map (kbd "RET") nil)
       ;; auto-complete compatibility
       (defun my-company-visible-and-explicit-action-p ()
         "Configure local behavior for company mode."
         (and (company-tooltip-visible-p)
              (company-explicit-action-p)))
       (defun company-ac-setup ()
         "Sets up `company-mode' to behave similarly to `auto-complete-mode'."
         (setq company-require-match nil)
         (setq company-auto-commit #'my-company-visible-and-explicit-action-p)
         (setq company-frontends '(company-echo-metadata-frontend
                                   company-pseudo-tooltip-unless-just-one-frontend-with-delay
                                   company-preview-frontend))
         (define-key company-active-map [tab]
                     'company-select-next-if-tooltip-visible-or-complete-selection)
         (define-key company-active-map (kbd "TAB")
                     'company-select-next-if-tooltip-visible-or-complete-selection))
       (company-ac-setup)
       (add-hook 'js2-mode-hook (lambda () (company-mode))))))
(use-package company-quickhelp :defer 30 :config (company-quickhelp-mode t))
(use-package company-web :defer t :config (add-to-list 'company-backends 'company-web-html))
(use-package company-jedi :defer t)
;;
;; autocomplete for creating source blocks in org files.
;; Fri Oct 15, 2021  4:27 PM
(use-package company-org-block :defer t)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			   end Company mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; intended to fix =cl is deprecated= errors
;; Sat Oct 16, 2021  2:52 PM
(use-package cl-libify :defer t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			begin Elpy for Python
;;		      Wed Jun 23, 2021  4:14 PM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(if (equal my-use-package-python-p t)
    (progn
      (use-package python-mode :defer t )
      ;; elpy
      ;; Wed Jun 23, 2021  3:05 AM
      (use-package elpy :defer t :init (elpy-enable)
        :config
        (require 'dap-python)
        (add-to-list 'company-backends 'company-jedi))
      ;; proper spacing around operators
        (message "*****ELPY enabled."))
  (message "***ELPY not enabled."))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			 end Elpy for Python
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq py-default-interpreter "c:/Python310/python.exe")

;; Sun Nov 21, 2021  2:57 PM
(use-package lsp-jedi :after elpy)
(add-to-list 'lsp-disabled-clients 'pyls)

;; (use-package flymake-python-pyflakes :defer t
;;   :hook (python-mode . flymake-python-pyflakes-load)
;;   )

;; linting
;; Thu Oct 14, 2021 10:03 AM
(global-flycheck-mode 1)
(use-package flycheck-pycheckers
  :init (setq flycheck-pycheckers-command "pycheckers.py"))
(with-eval-after-load 'flycheck
  (add-hook 'flycheck-mode-hook #'flycheck-pycheckers-setup))

;; Sun Nov 21, 2021  2:58 PM
(use-package python-black :after python
  :hook (python-mode . python-black-on-save-mode)
  :bind ("C-c p C-f" . 'python-black-buffer))

(use-package py-smart-operator :defer t)

;; moved to separate source block to speed loading.
;; Sun Nov 21, 2021  2:48 PM
;;
;; Sun Jul  4, 2021  6:34 PM
(use-package pydoc :defer t)
;; Sun Jul  4, 2021  6:34 PM
(use-package python-docstring :defer t :mode ("\\.py$" . python-mode))
;; Tue Jul 13, 2021  5:14 PM
;;
;; package deleted Sat Oct 30, 2021  8:22 PM
;; (use-package pippel :defer t :bind ("C-c m p" . pippel-list-packages))
;; m pippel-menu-mark-unmark remove mark
;; d pippel-menu-mark-delete mark for deletion
;; U pippel-menu-mark-all-upgrades mark all upgradable
;; u pippel-menu-mark-upgrade mark for upgrade
;; r pippel-list-packages refresh package list
;; i pippel-install-package prompt user for packages
;; x pippel-menu-execute perform marked package menu actions
;; RET pippel-menu-visit-homepage follow link

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			  begin emacs lisp
;;		      Fri Jul 16, 2021  3:12 PM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package adjust-parens :defer t)
;; (use-package elpl :defer t)
(use-package flylisp :defer t)
;;
;; package deleted Sat Oct 30, 2021  8:15 PM
;; (use-package el-spice :defer t )
;; (add-hook 'emacs-lisp-mode-hook 'el-spice-mode)
;; (add-hook 'lisp-interaction-mode-hook 'el-spice-mode)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			  end emacs lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;	      begin ESS - Emacs Speaks Statistics for R
;;		      Wed Dec 30, 2020  8:23 PM
;;  (add-to-list 'load-path "~/.emacs.default/elpa/ess-20201230.1711/lisp")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(if (equal my-use-package-ess-p t)
    (progn
      (use-package ess
        :defines company-active-map
        :after company
        :init (require 'ess-site))
      (use-package ess-smart-equals :defer t)
      (setq ess-use-company 'script-only) ;; this kills company-mode in R terminal
      (define-key company-active-map (kbd "C-c M-h") 'company-show-doc-buffer) ;; get help at point
      (message "*****ESS enabled."))
  (message "***ESS not enabled."))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;	       end ESS - Emacs Speaks Statistics for R
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			   begin Rust mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; edit Cargo files for Rust
;; Wed Dec 30, 2020  6:51 PM
;;
;; Set up Rust environment
;; Tue Jan  5, 2021 10:15 PM
(if (eq my-use-package-rust-p t)
    (progn
      (use-package cargo :defer t)
      (use-package rust-mode :defer t :init (setq rust-format-on-save t)
        :config (folding-mode)
        ;; (define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)
        (setq company-tooltip-align-annotations t)
        (with-eval-after-load 'rust-mode
          (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))
        :bind (
               :map rust-mode-map
                    ("C-c C-c" . 'cargo-process-run) ;; compile and run
                    ("C-c C-b" . 'cargo-process-build) ;; Build but do not run
                    ("C-M-|" . 'uncomment-region) ;; ESC-C-Shift-\
                    ("C-M-?" . 'comment-region) ;; ESC-C-Shift-/
                    ("TAB" . #'company-indent-or-complete-common)
                    :map global-map
                    ("C-c C-n" . cargo-process-new) ;; create new project
                    ))
      ;; force indentation to be spaces
      (add-hook 'rust-mode-hook (lambda () (setq indent-tabs-mode nil)))
      (use-package rustic :after rust-mode)
      ;; local version of the web-based scratchpad
      ;; Fri Nov 12, 2021  5:49 PM
      (use-package rust-playground :defer t)
      ;;
      ;; completions &c - how does this relate to LSP?
      ;; Fri Nov 12, 2021  6:02 PM
      (use-package racer :defer t)
      (add-hook 'rust-mode-hook #'racer-mode)
      (add-hook 'racer-mode-hook #'eldoc-mode)
      (add-hook 'racer-mode-hook #'company-mode)

      ;; add automatic completion of `use' statements
      ;; Tue Feb 22, 2022  9:35 PM
      (use-package rust-auto-use :defer t)

      (message "*****Rust mode enabled."))
  (message "***Rust mode not enabled."))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			    end Rust mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			    begin web mode
;;		      Thu Jun 24, 2021  9:27 PM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(if (eq my-use-package-web-p t)
    (progn
      (use-package auto-rename-tag :defer t)
      (use-package web-mode :defer t :config (auto-rename-tag-mode t) (folding-mode))
      (use-package emmet-mode :defer t
        :config
        (add-hook 'sgml-mode-hook 'emmet-mode)  ;; Auto-start on any markup modes
        (add-hook 'css-mode-hook  'emmet-mode)) ;; enable Emmet's css abbreviation
      (use-package web-mode-edit-element :defer t
        :hook (web-mode . web-mode-edit-element-minor-mode)
        (web-mode . (lambda () (html-check-frag-mode 1))))
      ;;(add-hook 'web-mode-hook 'web-mode-edit-element-minor-mode)
      (use-package html-check-frag :defer t)
      (use-package htmltagwrap :defer t
        :config
        (setq htmltagwrap-tag "div")
        (setq htmltagwrap-indent-region-after-wrap t)
        (setq htmltagwrap-indie-tag-wrap-not-inline t))
      (use-package vue-mode :defer t
        :config
        ;; 0, 1, or 2, representing (respectively) none, low, and high coloring
        (setq mmm-submode-decoration-level 1))
      (add-hook 'vue-mode-hook #'lsp-deferred)
      (setq mmm-global-mode 'maybe)
      (use-package vue-html-mode :defer t)
      ;; this guy tooks his packages off MELPA in a dispute over licensing
      ;; Thu Feb 24, 2022 10:16 AM
      ;; needed by xah-css-mode
      ;; (use-package xah-replace-pairs :defer t)
      ;; (use-package xah-css-mode :after xah-replace-pairs :defer t)
      (message "*****Web mode enabled."))
  (message "***Web mode not enabled."))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			     end web mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide '.prog-modes)
;;; .prog-modes.el ends here
