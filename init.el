;;; init.el --- Starting point of config loading -*- coding: utf-8; lexical-binding: t -*-
;;;
;;; Time-stamp: <2021-08-12 09:48:36 micha>
;;; Sun Sep 19, 2021 12:44 PM
;;; Default load file for emacs
;;; Sun Aug 30, 2015  8:58 AM
;;;
;;; Commentary:
;;;
;;; This file has cruft from a system that is not being used any longer
;;; FIXME At some point, set up init-loader
;;; Thu Jun 24, 2021 11:15 PM
;;;
;;; This file replaces the standard .emacs file that used to go into the home directory.
;;; It sets the custom-file variable, so that all changes through the customize interface
;;; go into the specified file
;;;
;;; (defvar my-use-package-p nil)
;;;
;;; (setq my-use-package-p t)
;;; (if (equal my-use-package-p t)
;;;     (message "true")
;;;   (message "false")
;;;     )

;;; Code:
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; do this to keep init.el in the git repo.
;;; Note: when cloning repo for new installation, this file must be
;;; copied to ~/.emacs.d/
;;; Thu Jul  1, 2021 10:36 AM
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; decrease number of garbage collecion calls while starting up
;; Sun Jul 11, 2021 12:46 PM
(setq gc-cons-threshold (* 50 1000 1000))

;; Thu Aug 12, 2021  9:48 AM
;; Move backups out of the way, per directory
(setq backup-directory-alist '(("." . "./.saves/")))

(defvar my-init-file "~/.emacs.d/init.el")
(defvar my-init-file-repo "~/.emacs.d/.emacs-config/init.el")
(defvar dotemacs-file-name "~/.emacs.d/.emacs-config/.emacs-config.el")

(defun efs/display-startup-time()
  "Measure the startup time for Emacs and display in *Messages* buffer."
  (message "Emacs loaded in %s with %d garbage collections."
           (format "%.2f seconds"
                   (float-time
                   (time-subtract after-init-time before-init-time)))
           gcs-done))

(add-hook 'emacs-startup-hook #'efs/display-startup-time)

(defun save-init()
  "Save a newer version of the init file to .emacs.d before exiting.
If the currently edited file is newer, copy to its working
location.  Set `OK-IF-ALREADY-EXISTS' flag to t.  Attach the function
to the `kill-emacs-hook'.  This hook is called when Emacs is closed."
  (if (file-newer-than-file-p my-init-file-repo my-init-file)
  (copy-file my-init-file-repo my-init-file t))
  )

(add-hook 'kill-emacs-hook 'save-init)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;			 end of init handling
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
(require 'package)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; make sure it's installed before requiring it.
;; Sun Jul 11, 2021 12:12 PM
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
;;
;; wrapped this per suggestion on `use-package' web site
;; Fri Oct  8, 2021 10:54 PM
(eval-when-compile
  (require 'use-package))

;; added to overcome issue of not being installed on new system.
;; Wed Sep  8, 2021  9:56 AM
(unless (package-installed-p 'bind-key)
  (package-install 'bind-key))

(require 'bind-key)

(setq use-package-compute-statistics t)

;; detailed info in the Messages buffer
(setq use-package-verbose t)

;; Fri Oct 8, 2021 12:28 PM
;; Commented out because of issues with a
;; broken package update.  Specifically, helm broke and I wasn't able
;; to get into these files to fix it. Not that big of a deal to
;; upgrade manually. Had to exit and use another editor! Lordy!

;; (unless (package-installed-p 'auto-package-update)
;;   (package-install 'auto-package-update))

;; (require 'auto-package-update)
;; (auto-package-update-maybe)

;; Set the time stamp function to track editing of these files.
;; Sat Jan  2, 2021  6:03 PM
(add-hook 'write-file-functions 'time-stamp)
(message "setting custom file.")
(setq custom-file "~/.emacs.d/.emacs-config/custom.el")
(load custom-file)
;; make it possible to upcase a region - this is off by default
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;; turn on native comp
;;
(setq native-comp-deferred-compilation t)

;; load-path
;; Sun Nov 21, 2021 10:57 AM
(add-to-list 'load-path "~/.emacs.d/.emacs-config")

;; change some font locking for emacs lisp, I don't like some of
;; the defaults.
;; Fri Jul 30, 2021  9:23 PM
(font-lock-add-keywords 'emacs-lisp-mode
                        '(("\\<\\(add-to-list\\|file-newer-than-file-p\\|byte-compile-file\\|file-exists-p\\|add-hook\\|concat\\)\\>"
                           . font-lock-function-name-face)
                          ("\\<\\(put\\|message\\|and\\|or\\|not\\)\\>" . font-lock-keyword-face)
                          )
                        )

;; (font-lock-add-keywords 'c-mode
;; 			'(("\\<\\(FIXME\\):" 1 font-lock-warning-face prepend)
;; 			  ("\\<\\(and\\|or\\|not\\)\\>" . font-lock-keyword-face)
;;
;; 			  )
;;
;; 			)

;; set these separately from the old system because I use them to
;; byte-compile newly edited files.
;; Sun Nov 21, 2021 11:19 AM

;; Sat Feb 27, 2021 11:14 AM
(defvar config-vars-source "~/.emacs.d/.emacs-config/.emacs-config-vars.el")
;; (defvar config-func-source "~/.emacs.d/.emacs-config/.emacs-config-func.el")
(defvar config-main-source "~/.emacs.d/.emacs-config/.emacs-config.el")
(defvar prog-modes-source  "~/.emacs.d/.emacs-config/.prog-modes.el")

;; simply turns the source filenames into compiled filenames.
(defvar prog-modes-compiled  (concat prog-modes-source "c"))
(defvar config-vars-compiled (concat config-vars-source "c"))
;; (defvar config-func-compiled (concat config-func-source "c"))
(defvar config-main-compiled (concat config-main-source "c"))

;; alternate load of config files to test speed
;; Sun Nov 21, 2021 10:59 AM

(message "=-=-=loading vars file (new method)")
(load ".emacs-config-vars")
(message "=-=-=vars file loaded (new method)")

(message "=-=-=loading main file (new method)")
(load ".emacs-config")
(message "=-=-=main file loaded (new method)")

(message "=-=-=loading prog modes file (new method)")
(load ".prog-modes")
(message "=-=-=prog modes file loaded (new method)")

;; set this back down after loading
;; Sun Jul 11, 2021 12:48 PM
(setq gc-cons-threshold (* 2 1000 1000))

(provide 'init)

;;; init.el ends here
