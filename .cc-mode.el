;;; .cc-mode.el --- Configuration for using cc-mode -*- coding: utf-8 -*-
;;			  $Author: mpowe $
;;		     $Date: 2001/05/06 02:57:37 $
;;			   $Revision: 1.15 $
;;		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Editing Options for C mode          ;;
;; Tue Apr 27, 1999  1:51 PM           ;;
;; Moved from .emacs to a separate     ;;
;; file Mon May 17, 1999 11:00 PM      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;

;;; Commentary:
;;; These settings configure `cc-mode' for my particular usage.

(require 'cc-mode)

;;; Code:

(autoload 'align "align" nil t)
;;
;; Use this for adding headers to a new file
;;
(defun my-c-headers()
  "Add certain default header files to a new code file."
  (interactive)
  (insert "#include <stdio.h>\n#include <stdlib.h>\n"))
;;
;; mode-compile
;; add here for a test run
;; Wed Nov 10, 1999  3:48 PM
;;
(autoload 'mode-compile "mode-compile"
  "Command to compile current buffer file based on the major mode" t)
(global-set-key "\C-cc" 'mode-compile)
(autoload 'mode-compile-kill "mode-compile"
  "Command to kill a compilation launched by `mode-compile'" t)
(global-set-key "\C-ck" 'mode-compile-kill)
;;
;; Define some local keybindings for my C mode
;;
;; Sat Jan 28, 2006 11:42 AM
;; changed indentation to 4 spaces from 2
;;
(defconst my-c-style
  '(
  (c-offsets-alist . ((case-label . 4)
		      (defun-block-intro . 4)
		      (statement-block-intro . 4))))
  "My C Programming Style.")
;;
(defun my-c-mode-hook()
  "A hook function for setting certain C-specific settings."
  (turn-on-font-lock)
  (c-toggle-auto-hungry-state 1)
  (c-set-offset 'defun-block-intro 4)
  (c-set-offset 'statement-block-intro 4)
  (define-key c-mode-base-map "\C-ca" "->")
  (define-key c-mode-base-map "\C-cb" 'c-beginning-of-defun)
  (define-key c-mode-base-map "\C-cd" "#define ")
  (define-key c-mode-base-map "\C-ce" "#include ")
  (define-key c-mode-base-map "\C-cf" 'c-end-of-defun)
  (define-key c-mode-base-map "\C-ci" 'indent-region)
  (define-key c-mode-base-map "\C-ch" 'my-c-headers)
  (define-key c-mode-base-map "\C-cj" 'goto-line)
  (define-key c-mode-base-map [f8] 'compile)
  (define-key c-mode-base-map [f9] 'recompile)
  (define-key c-mode-base-map [f10] 'speedbar)
  (c-add-style "PERSONAL" my-c-style t))
(add-hook 'c-mode-common-hook 'my-c-mode-hook)

;; test of resetting of compile command

(defun my-gcc-setup()
  "Default settings for gcc."
  (setq compile-command (format "gcc -o %s %s" (file-name-sans-extension buffer-file-name) (buffer-file-name))))
(add-hook 'c-mode-hook 'my-gcc-setup)

;;
;; end cc-mode configuration
;;

(provide '.cc-mode)

;;; .cc-mode.el ends here
